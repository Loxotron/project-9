@echo off
setlocal

rem == Setup path to nmake.exe, from vc 2005 common tools directory ==
call "%VS120COMNTOOLS%vsvars32.bat"

rem ================================
rem ==== MOD PATH CONFIGURATIONS ===

rem == Set the absolute path to your mod's game directory here ==
set GAMEDIR="C:\Program Files (x86)\Steam\steamapps\SourceMods\Project9"
rem set GAMEDIR="E:\Steam\SteamApps\common\Missing Information\MissingInfo"

rem == Set the relative or absolute path to Source SDK Base 2013 Singleplayer\bin ==
set SDKBINDIR=C:\hl2modcompile\steamapps\common\sdk2006\bin

rem ==  Set the Path to your mod's root source code ==
rem This should already be correct, accepts relative paths only!
set SOURCEDIR=..\..

rem ==== MOD PATH CONFIGURATIONS END ===
rem ====================================


call buildsdkshaders.bat stdshader_dx9

pause