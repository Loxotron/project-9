class sdk_core_vs20_Static_Index
{
private:
	int m_nMODEL;
#ifdef _DEBUG
	bool m_bMODEL;
#endif
public:
	void SetMODEL( int i )
	{
		Assert( i >= 0 && i <= 1 );
		m_nMODEL = i;
#ifdef _DEBUG
		m_bMODEL = true;
#endif
	}
	void SetMODEL( bool i )
	{
		m_nMODEL = i ? 1 : 0;
#ifdef _DEBUG
		m_bMODEL = true;
#endif
	}
public:
	sdk_core_vs20_Static_Index()
	{
#ifdef _DEBUG
		m_bMODEL = false;
#endif // _DEBUG
		m_nMODEL = 0;
	}
	int GetIndex()
	{
		// Asserts to make sure that we aren't using any skipped combinations.
		// Asserts to make sure that we are setting all of the combination vars.
#ifdef _DEBUG
		bool bAllStaticVarsDefined = m_bMODEL;
		Assert( bAllStaticVarsDefined );
#endif // _DEBUG
		return ( 4 * m_nMODEL ) + 0;
	}
};
#define shaderStaticTest_sdk_core_vs20 vsh_forgot_to_set_static_MODEL + 0
class sdk_core_vs20_Dynamic_Index
{
private:
	int m_nNUMBONES;
#ifdef _DEBUG
	bool m_bNUMBONES;
#endif
public:
	void SetNUMBONES( int i )
	{
		Assert( i >= 0 && i <= 3 );
		m_nNUMBONES = i;
#ifdef _DEBUG
		m_bNUMBONES = true;
#endif
	}
	void SetNUMBONES( bool i )
	{
		m_nNUMBONES = i ? 1 : 0;
#ifdef _DEBUG
		m_bNUMBONES = true;
#endif
	}
public:
	sdk_core_vs20_Dynamic_Index()
	{
#ifdef _DEBUG
		m_bNUMBONES = false;
#endif // _DEBUG
		m_nNUMBONES = 0;
	}
	int GetIndex()
	{
		// Asserts to make sure that we aren't using any skipped combinations.
		// Asserts to make sure that we are setting all of the combination vars.
#ifdef _DEBUG
		bool bAllDynamicVarsDefined = m_bNUMBONES;
		Assert( bAllDynamicVarsDefined );
#endif // _DEBUG
		return ( 1 * m_nNUMBONES ) + 0;
	}
};
#define shaderDynamicTest_sdk_core_vs20 vsh_forgot_to_set_dynamic_NUMBONES + 0
