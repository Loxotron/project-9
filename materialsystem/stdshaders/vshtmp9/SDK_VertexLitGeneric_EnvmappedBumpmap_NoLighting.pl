push @output, "vs.1.1																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(1)\n";
# DYNAMIC: "DOWATERFOG"				"0..1"																								
# DYNAMIC: "SKINNING"				"0..1"	[XBOX]																								
# DYNAMIC: "NUM_BONES"				"0..3"	[PC]																								
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(7)\n";
push @output, "; Shader specific constant:																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(8)\n";
 	 push @output, ";	 $SHADER_SPECIFIC_CONST_5	 = [sOffset, tOffset, 0, 0]																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(9)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(10)\n";
push @output, ";------------------------------------																								; LINEINFO(SDK_macros.vsh)(1)\n";
push @output, "; RULES FOR AUTHORING VERTEX SHADERS:																								; LINEINFO(SDK_macros.vsh)(2)\n";
push @output, ";------------------------------------																								; LINEINFO(SDK_macros.vsh)(3)\n";
push @output, "; - never use \"def\" . . .set constants in code instead. . our constant shadowing will break otherwise.																								; LINEINFO(SDK_macros.vsh)(4)\n";
 	push @output, ";	(same goes for pixel shaders)																								; LINEINFO(SDK_macros.vsh)(5)\n";
push @output, "; - use cN notation instead of c[N] notation. .makes grepping for registers easier.																								; LINEINFO(SDK_macros.vsh)(6)\n";
    push @output, ";   The only exception is c[a0.x+blah] where you have no choice.																								; LINEINFO(SDK_macros.vsh)(7)\n";
$g_NumRegisters = 12;																								
push @output, "; NOTE: These must match the same values in vsh_prep.pl!																								; LINEINFO(SDK_macros.vsh)(10)\n";
$vPos				= "v0";																								
$vBoneWeights		= "v1";																								
$vBoneIndices		= "v2";																								
$vNormal			= "v3";																								
if ( $g_xbox )																								
{																								
	$vPosFlex		= "v4";																								
	$vNormalFlex	= "v13";																								
}																								
$vColor				= "v5";																								
$vSpecular			= "v6";																								
$vTexCoord0			= "v7";																								
$vTexCoord1			= "v8";																								
$vTexCoord2			= "v9";																								
$vTexCoord3			= "v10";																								
$vTangentS			= "v11";																								
$vTangentT			= "v12";																								
$vUserData			= "v14";																								
if( $g_dx9 )																								
{																								
	if( $g_usesPos )																								
	{																								
		push @output, "		dcl_position $vPos;																								; LINEINFO(SDK_macros.vsh)(34)\n";
	}																								
	if ( $g_xbox )																								
	{																								
		if ( $g_usesPosFlex )																								
		{																								
			push @output, "			dcl_position1 $vPosFlex;																								; LINEINFO(SDK_macros.vsh)(41)\n";
		}																								
		if( $g_usesNormalFlex )																								
		{																								
			push @output, "			dcl_normal1 $vNormalFlex;																								; LINEINFO(SDK_macros.vsh)(45)\n";
		}																								
	}																								
	if( $g_usesBoneWeights )																								
	{																								
		push @output, "		dcl_blendweight $vBoneWeights;																								; LINEINFO(SDK_macros.vsh)(51)\n";
	}																								
	if( $g_usesBoneIndices )																								
	{																								
		push @output, "		dcl_blendindices $vBoneIndices;																								; LINEINFO(SDK_macros.vsh)(55)\n";
	}																								
	if( $g_usesNormal )																								
	{																								
		push @output, "		dcl_normal $vNormal;																								; LINEINFO(SDK_macros.vsh)(59)\n";
	}																								
	if( $g_usesColor )																								
	{																								
		push @output, "		dcl_color0 $vColor;																								; LINEINFO(SDK_macros.vsh)(63)\n";
	}																								
	if( $g_usesSpecular )																								
	{																								
		push @output, "		dcl_color1 $vSpecular;																								; LINEINFO(SDK_macros.vsh)(67)\n";
	}																								
	if( $g_usesTexCoord0 )																								
	{																								
		push @output, "		dcl_texcoord0 $vTexCoord0;																								; LINEINFO(SDK_macros.vsh)(71)\n";
	}																								
	if( $g_usesTexCoord1 )																								
	{																								
		push @output, "		dcl_texcoord1 $vTexCoord1;																								; LINEINFO(SDK_macros.vsh)(75)\n";
	}																								
	if( $g_usesTexCoord2 )																								
	{																								
		push @output, "		dcl_texcoord2 $vTexCoord2;																								; LINEINFO(SDK_macros.vsh)(79)\n";
	}																								
	if( $g_usesTexCoord3 )																								
	{																								
		push @output, "		dcl_texcoord3 $vTexCoord3;																								; LINEINFO(SDK_macros.vsh)(83)\n";
	}																								
	if( $g_usesTangentS )																								
	{																								
		push @output, "		dcl_tangent $vTangentS;																								; LINEINFO(SDK_macros.vsh)(87)\n";
	}																								
	if( $g_usesTangentT )																								
	{																								
		push @output, "		dcl_binormal0 $vTangentT;																								; LINEINFO(SDK_macros.vsh)(91)\n";
	}																								
	if( $g_usesUserData )																								
	{																								
		push @output, "		dcl_tangent $vUserData;																								; LINEINFO(SDK_macros.vsh)(95)\n";
	}																								
}																								
# NOTE: These should match g_LightCombinations in vertexshaderdx8.cpp!																								
# NOTE: Leave this on single lines or shit might blow up.																								
@g_staticLightTypeArray = ( "none", "static", "none", "none", "none", "none", "none", "none", "none", "none", "none", "none", "static", "static", "static", "static", "static", "static", "static", "static", "static", "static" );																								
@g_ambientLightTypeArray = ( "none", "none", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", 	"ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient", "ambient" );																								
@g_localLightType1Array = ( "none", "none", "none", "spot", "point", "directional", "spot", "spot", "spot", "point", "point", "directional", "none", "spot", "point", "directional", "spot", "spot", "spot", "point", "point", "directional" );																								
@g_localLightType2Array = ( "none", "none", "none", "none", "none", "none", "spot", "point", "directional", "point", "directional", "directional", "none", "none", "none", "none", "spot", "point", "directional", "point", "directional", "directional" );																								
$cConstants0		= "c0";																								
$cZero				= "c0.x";																								
$cOne				= "c0.y";																								
$cTwo				= "c0.z";																								
$cHalf				= "c0.w";																								
$cConstants1		    = "c1";																								
$cOOGamma			    = "c1.x"; # 1/2.2																								
$cOtherOverbrightFactor = "c1.y"; # overbright																								
$cOneThird			    = "c1.z"; # 1/3																								
$cOverbrightFactor      = "c1.w"; # 1/overbright																								
$cEyePos			= "c2";																								
$cWaterZ			= "c2.w";																								
$cEyePosWaterZ		= "c2";																								
$cLightIndex		= "c3";																								
$cLight0Offset		= "c3.x"; # 27																								
$cLight1Offset		= "c3.y"; # 32																								
$cColorToIntScale	= "c3.z"; # matrix array offset = 3.0f * 255.0f + 0.01 (epsilon ensures floor yields desired result)																								
$cModel0Index		= "c3.w"; # base for start of skinning matrices																								
push @output, "; NOTE: These must match the same values in vsh_prep.pl!																								; LINEINFO(SDK_macros.vsh)(128)\n";
$cModelViewProj0	= "c4";																								
$cModelViewProj1	= "c5";																								
$cModelViewProj2	= "c6";																								
$cModelViewProj3	= "c7";																								
$cViewProj0			= "c8";																								
$cViewProj1			= "c9";																								
$cViewProj2			= "c10";																								
$cViewProj3			= "c11";																								
$SHADER_HALFLAMBERT = "c12.x";																								
$SHADER_FLEXSCALE	= "c13.x";																								
push @output, "; currently unused																								; LINEINFO(SDK_macros.vsh)(141)\n";
push @output, "; c14, c15																								; LINEINFO(SDK_macros.vsh)(142)\n";
$cFogParams			= "c16";																								
$cFogEndOverFogRange = "c16.x";																								
$cFogOne			= "c16.y";																								
push @output, "; NOTE: c16.z is unused																								; LINEINFO(SDK_macros.vsh)(147)\n";
$cOOFogRange		= "c16.w"; # (1/(fogEnd-fogStart))																								
$cViewModel0		= "c17";																								
$cViewModel1		= "c18";																								
$cViewModel2		= "c19";																								
$cViewModel3		= "c20";																								
$cAmbientColorPosX	= "c21";																								
$cAmbientColorNegX	= "c22";																								
$cAmbientColorPosY	= "c23";																								
$cAmbientColorNegY	= "c24";																								
$cAmbientColorPosZ	= "c25";																								
$cAmbientColorNegZ	= "c26";																								
$cAmbientColorPosXOffset = "21";																								
$cAmbientColorPosYOffset = "23";																								
$cAmbientColorPosZOffset = "25";																								
$cLight0DiffColor	= "c27";																								
$cLight0Dir			= "c28";																								
$cLight0Pos			= "c29";																								
$cLight0SpotParams  = "c30"; # [ exponent, stopdot, stopdot2, 1 / (stopdot - stopdot2)																								
$cLight0Atten		= "c31"; # [ constant, linear, quadratic, 0.0f ]																								
$cLight1DiffColor	= "c32";																								
$cLight1Dir			= "c33";																								
$cLight1Pos			= "c34";																								
$cLight1SpotParams  = "c35"; # [ exponent, stopdot, stopdot2, 1 / (stopdot - stopdot2)																								
$cLight1Atten		= "c36"; # [ constant, linear, quadratic, 0.0f ]																								
$cModulationColor	= "c37";																								
$SHADER_SPECIFIC_CONST_0  = "c38";																								
$SHADER_SPECIFIC_CONST_1  = "c39";																								
$SHADER_SPECIFIC_CONST_2  = "c40";																								
$SHADER_SPECIFIC_CONST_3  = "c41";																								
$SHADER_SPECIFIC_CONST_4  = "c42";																								
$SHADER_SPECIFIC_CONST_5  = "c43";																								
$SHADER_SPECIFIC_CONST_6  = "c44";																								
$SHADER_SPECIFIC_CONST_7  = "c45";																								
$SHADER_SPECIFIC_CONST_8  = "c46";																								
$SHADER_SPECIFIC_CONST_9  = "c47";																								
push @output, "; xbox reserved contants for viewport transform																								; LINEINFO(SDK_macros.vsh)(191)\n";
push @output, "; enabled extended range [-96..-1] for use by matrix palette skinning																								; LINEINFO(SDK_macros.vsh)(192)\n";
push @output, "; using extended range forced the clobbering of xbox viewport constants c-38,c-37																								; LINEINFO(SDK_macros.vsh)(193)\n";
push @output, "; force xbox reserved to top of range to allow matix skinning a contiguous range																								; LINEINFO(SDK_macros.vsh)(194)\n";
$SHADER_VIEWPORT_CONST_OFFSET = "c-1";																								
$SHADER_VIEWPORT_CONST_SCALE  = "c-2";																								
push @output, "; There are 16 model matrices for skinning																								; LINEINFO(SDK_macros.vsh)(198)\n";
push @output, "; NOTE: These must match the same values in vsh_prep.pl!																								; LINEINFO(SDK_macros.vsh)(199)\n";
$cModel0			= "c48";																								
$cModel1			= "c49";																								
$cModel2			= "c50";																								
sub OutputUsedRegisters																								
{																								
	local( $i );																								
	push @output, "	; USED REGISTERS																								; LINEINFO(SDK_macros.vsh)(207)\n";
	for( $i = 0; $i < $g_NumRegisters; $i++ )																								
	{																								
		if( $g_allocated[$i] )																								
		{																								
			push @output, "			; $g_allocatedname[$i] = r$i																								; LINEINFO(SDK_macros.vsh)(212)\n";
		}																								
	}																								
	push @output, "	;																								; LINEINFO(SDK_macros.vsh)(215)\n";
}																								
sub AllocateRegister																								
{																								
	local( *reg ) = shift;																								
	local( $regname ) = shift;																								
	local( $i );																								
	for( $i = 0; $i < $g_NumRegisters; $i++ )																								
	{																								
		if( !$g_allocated[$i] )																								
		{																								
			$g_allocated[$i] = 1;																								
			$g_allocatedname[$i] = $regname;																								
			push @output, "			; AllocateRegister $regname = r$i																								; LINEINFO(SDK_macros.vsh)(229)\n";
			$reg = "r$i";																								
			&OutputUsedRegisters();																								
			return;																								
		}																								
	}																								
	push @output, "	; Out of registers allocating $regname!																								; LINEINFO(SDK_macros.vsh)(235)\n";
	$reg = "rERROR_OUT_OF_REGISTERS";																								
	&OutputUsedRegisters();																								
}																								
push @output, "; pass in a reference to a var that contains a register. . ie \$var where var will constain \"r1\", etc																								; LINEINFO(SDK_macros.vsh)(240)\n";
sub FreeRegister																								
{																								
	local( *reg ) = shift;																								
	local( $regname ) = shift;																								
	push @output, "	; FreeRegister $regname = $reg																								; LINEINFO(SDK_macros.vsh)(245)\n";
	if( $reg =~ m/rERROR_DEALLOCATED/ )																								
	{																								
		push @output, "		; $regname already deallocated																								; LINEINFO(SDK_macros.vsh)(248)\n";
		push @output, "		; $reg = \"rALREADY_DEALLOCATED\";																								; LINEINFO(SDK_macros.vsh)(249)\n";
		&OutputUsedRegisters();																								
		return;																								
	}																								
 	push @output, ";	if( $regname ne g_allocatedname[$reg] )																								; LINEINFO(SDK_macros.vsh)(253)\n";
 	push @output, ";	{																								; LINEINFO(SDK_macros.vsh)(254)\n";
 		push @output, ";		; Error freeing $reg																								; LINEINFO(SDK_macros.vsh)(255)\n";
 		push @output, ";		mov compileerror, freed unallocated register $regname																								; LINEINFO(SDK_macros.vsh)(256)\n";
 	push @output, ";	}																								; LINEINFO(SDK_macros.vsh)(257)\n";
	if( ( $reg =~ m/r(.*)/ ) )																								
	{																								
		$g_allocated[$1] = 0;																								
	}																								
	$reg = "rERROR_DEALLOCATED";																								
	&OutputUsedRegisters();																								
}																								
sub CheckUnfreedRegisters()																								
{																								
	local( $i );																								
	for( $i = 0; $i < $g_NumRegisters; $i++ )																								
	{																								
		if( $g_allocated[$i] )																								
		{																								
			print "ERROR: r$i allocated to $g_allocatedname[$i] at end of program\n";																								
			$g_allocated[$i] = 0;																								
		}																								
	}																								
}																								
sub Normalize																								
{																								
	local( $r ) = shift;																								
	push @output, "	dp3 $r.w, $r, $r																								; LINEINFO(SDK_macros.vsh)(283)\n";
	push @output, "	rsq $r.w, $r.w																								; LINEINFO(SDK_macros.vsh)(284)\n";
	push @output, "	mul $r, $r, $r.w																								; LINEINFO(SDK_macros.vsh)(285)\n";
}																								
sub Cross																								
{																								
	local( $result ) = shift;																								
	local( $a ) = shift;																								
	local( $b ) = shift;																								
	push @output, "	mul $result.xyz, $a.yzx, $b.zxy																								; LINEINFO(SDK_macros.vsh)(294)\n";
	push @output, "	mad $result.xyz, -$b.yzx, $a.zxy, $result																								; LINEINFO(SDK_macros.vsh)(295)\n";
}																								
sub RangeFog																								
{																								
	local( $projPos ) = shift;																								
	push @output, "	;------------------------------																								; LINEINFO(SDK_macros.vsh)(302)\n";
	push @output, "	; Regular range fog																								; LINEINFO(SDK_macros.vsh)(303)\n";
	push @output, "	;------------------------------																								; LINEINFO(SDK_macros.vsh)(304)\n";
	push @output, "	; oFog.x = 1.0f = no fog																								; LINEINFO(SDK_macros.vsh)(306)\n";
	push @output, "	; oFog.x = 0.0f = full fog																								; LINEINFO(SDK_macros.vsh)(307)\n";
	push @output, "	; compute fog factor f = (fog_end - dist)*(1/(fog_end-fog_start))																								; LINEINFO(SDK_macros.vsh)(308)\n";
	push @output, "	; this is == to: (fog_end/(fog_end-fog_start) - dist/(fog_end-fog_start)																								; LINEINFO(SDK_macros.vsh)(309)\n";
	push @output, "	; which can be expressed with a single mad instruction!																								; LINEINFO(SDK_macros.vsh)(310)\n";
	push @output, "	; Compute |projPos|																								; LINEINFO(SDK_macros.vsh)(312)\n";
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	dp3 $tmp.x, $projPos.xyw, $projPos.xyw																								; LINEINFO(SDK_macros.vsh)(315)\n";
	push @output, "	rsq $tmp.x, $tmp.x																								; LINEINFO(SDK_macros.vsh)(316)\n";
	push @output, "	rcp $tmp.x, $tmp.x																								; LINEINFO(SDK_macros.vsh)(317)\n";
	if( $g_dx9 )																								
	{																								
		push @output, "		mad $tmp, -$tmp.x, $cOOFogRange, $cFogEndOverFogRange																								; LINEINFO(SDK_macros.vsh)(321)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(322)\n";
		push @output, "		max oFog, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(323)\n";
	}																								
	else																								
	{																								
		push @output, "		mad $tmp, -$tmp.x, $cOOFogRange, $cFogEndOverFogRange																								; LINEINFO(SDK_macros.vsh)(327)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(328)\n";
		push @output, "		max oFog.x, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(329)\n";
	}																								
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub DepthFog																								
{																								
	local( $projPos ) = shift;																								
	local( $dest ) = shift;																								
	if ( $dest eq "" )																								
	{																								
		$dest = "oFog";																								
	}																								
	push @output, "	;------------------------------																								; LINEINFO(SDK_macros.vsh)(344)\n";
	push @output, "	; Regular range fog																								; LINEINFO(SDK_macros.vsh)(345)\n";
	push @output, "	;------------------------------																								; LINEINFO(SDK_macros.vsh)(346)\n";
	push @output, "	; oFog.x = 1.0f = no fog																								; LINEINFO(SDK_macros.vsh)(348)\n";
	push @output, "	; oFog.x = 0.0f = full fog																								; LINEINFO(SDK_macros.vsh)(349)\n";
	push @output, "	; compute fog factor f = (fog_end - dist)*(1/(fog_end-fog_start))																								; LINEINFO(SDK_macros.vsh)(350)\n";
	push @output, "	; this is == to: (fog_end/(fog_end-fog_start) - dist/(fog_end-fog_start)																								; LINEINFO(SDK_macros.vsh)(351)\n";
	push @output, "	; which can be expressed with a single mad instruction!																								; LINEINFO(SDK_macros.vsh)(352)\n";
	push @output, "	; Compute |projPos|																								; LINEINFO(SDK_macros.vsh)(354)\n";
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	if( $g_dx9 )																								
	{																								
		push @output, "		mad $tmp, -$projPos.w, $cOOFogRange, $cFogEndOverFogRange																								; LINEINFO(SDK_macros.vsh)(360)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(361)\n";
		push @output, "		max $dest, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(362)\n";
	}																								
	else																								
	{																								
		push @output, "		mad $tmp, -$projPos.w, $cOOFogRange, $cFogEndOverFogRange																								; LINEINFO(SDK_macros.vsh)(366)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(367)\n";
		push @output, "		max $dest.x, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(368)\n";
	}																								
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub WaterRangeFog																								
{																								
	push @output, "	; oFog.x = 1.0f = no fog																								; LINEINFO(SDK_macros.vsh)(376)\n";
	push @output, "	; oFog.x = 0.0f = full fog																								; LINEINFO(SDK_macros.vsh)(377)\n";
	push @output, "	; only $worldPos.z is used out of worldPos																								; LINEINFO(SDK_macros.vsh)(379)\n";
	local( $worldPos ) = shift;																								
	local( $projPos ) = shift;																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; This is simple similar triangles. Imagine a line passing from the point directly vertically																								; LINEINFO(SDK_macros.vsh)(386)\n";
	push @output, "	; and another line passing from the point to the eye position.																								; LINEINFO(SDK_macros.vsh)(387)\n";
	push @output, "	; Let d = total distance from point to the eye																								; LINEINFO(SDK_macros.vsh)(388)\n";
	push @output, "	; Let h = vertical distance from the point to the eye																								; LINEINFO(SDK_macros.vsh)(389)\n";
	push @output, "	; Let hw = vertical distance from the point to the water surface																								; LINEINFO(SDK_macros.vsh)(390)\n";
	push @output, "	; Let dw = distance from the point to a point on the water surface that lies along the ray from point to eye																								; LINEINFO(SDK_macros.vsh)(391)\n";
	push @output, "	; Therefore d/h = dw/hw by similar triangles, or dw = d * hw / h.																								; LINEINFO(SDK_macros.vsh)(392)\n";
	push @output, "	; d = |projPos|, h = eyepos.z - worldPos.z, hw = waterheight.z - worldPos.z, dw = what we solve for																								; LINEINFO(SDK_macros.vsh)(393)\n";
	push @output, "	; Now, tmp.x = hw, and tmp.y = h																								; LINEINFO(SDK_macros.vsh)(395)\n";
	push @output, "	add $tmp.xy, $cEyePosWaterZ.wz, -$worldPos.z																								; LINEINFO(SDK_macros.vsh)(396)\n";
	push @output, "	; if $tmp.x < 0, then set it to 0																								; LINEINFO(SDK_macros.vsh)(398)\n";
	push @output, "	; This is the equivalent of moving the vert to the water surface if it's above the water surface																								; LINEINFO(SDK_macros.vsh)(399)\n";
	push @output, "	max $tmp.x, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(400)\n";
	push @output, "	; Compute 1 / |projPos| = 1/d																								; LINEINFO(SDK_macros.vsh)(402)\n";
	push @output, "	dp3 $tmp.z, $projPos.xyw, $projPos.xyw																								; LINEINFO(SDK_macros.vsh)(403)\n";
	push @output, "	rsq $tmp.z, $tmp.z																								; LINEINFO(SDK_macros.vsh)(404)\n";
	push @output, "	; Now we have h/d																								; LINEINFO(SDK_macros.vsh)(406)\n";
	push @output, "	mul $tmp.z, $tmp.z, $tmp.y																								; LINEINFO(SDK_macros.vsh)(407)\n";
	push @output, "	; Now we have d/h																								; LINEINFO(SDK_macros.vsh)(409)\n";
	push @output, "	rcp $tmp.w, $tmp.z																								; LINEINFO(SDK_macros.vsh)(410)\n";
	push @output, "	; We finally have d * hw / h																								; LINEINFO(SDK_macros.vsh)(412)\n";
	push @output, "	; $tmp.w is now the distance that we see through water.																								; LINEINFO(SDK_macros.vsh)(413)\n";
	push @output, "	mul $tmp.w, $tmp.x, $tmp.w																								; LINEINFO(SDK_macros.vsh)(414)\n";
	if( $g_dx9 )																								
	{																								
		push @output, "		mad $tmp, -$tmp.w, $cOOFogRange, $cFogOne																								; LINEINFO(SDK_macros.vsh)(418)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(419)\n";
		push @output, "		max oFog, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(420)\n";
	}																								
	else																								
	{																								
		push @output, "		mad $tmp, -$tmp.w, $cOOFogRange, $cFogOne																								; LINEINFO(SDK_macros.vsh)(424)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(425)\n";
		push @output, "		max oFog.x, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(426)\n";
	}																								
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub WaterDepthFog																								
{																								
	push @output, "	; oFog.x = 1.0f = no fog																								; LINEINFO(SDK_macros.vsh)(434)\n";
	push @output, "	; oFog.x = 0.0f = full fog																								; LINEINFO(SDK_macros.vsh)(435)\n";
	push @output, "	; only $worldPos.z is used out of worldPos																								; LINEINFO(SDK_macros.vsh)(437)\n";
	local( $worldPos ) = shift;																								
	local( $projPos ) = shift;																								
	local( $dest ) = shift;																								
	if ( $dest eq "" )																								
	{																								
		$dest = "oFog";																								
	}																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; This is simple similar triangles. Imagine a line passing from the point directly vertically																								; LINEINFO(SDK_macros.vsh)(450)\n";
	push @output, "	; and another line passing from the point to the eye position.																								; LINEINFO(SDK_macros.vsh)(451)\n";
	push @output, "	; Let d = total distance from point to the eye																								; LINEINFO(SDK_macros.vsh)(452)\n";
	push @output, "	; Let h = vertical distance from the point to the eye																								; LINEINFO(SDK_macros.vsh)(453)\n";
	push @output, "	; Let hw = vertical distance from the point to the water surface																								; LINEINFO(SDK_macros.vsh)(454)\n";
	push @output, "	; Let dw = distance from the point to a point on the water surface that lies along the ray from point to eye																								; LINEINFO(SDK_macros.vsh)(455)\n";
	push @output, "	; Therefore d/h = dw/hw by similar triangles, or dw = d * hw / h.																								; LINEINFO(SDK_macros.vsh)(456)\n";
	push @output, "	; d = projPos.w, h = eyepos.z - worldPos.z, hw = waterheight.z - worldPos.z, dw = what we solve for																								; LINEINFO(SDK_macros.vsh)(457)\n";
	push @output, "	; Now, tmp.x = hw, and tmp.y = h																								; LINEINFO(SDK_macros.vsh)(459)\n";
	push @output, "	add $tmp.xy, $cEyePosWaterZ.wz, -$worldPos.z																								; LINEINFO(SDK_macros.vsh)(460)\n";
	push @output, "	; if $tmp.x < 0, then set it to 0																								; LINEINFO(SDK_macros.vsh)(462)\n";
	push @output, "	; This is the equivalent of moving the vert to the water surface if it's above the water surface																								; LINEINFO(SDK_macros.vsh)(463)\n";
	push @output, "	max $tmp.x, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(464)\n";
	push @output, "	; Now we have 1/h																								; LINEINFO(SDK_macros.vsh)(466)\n";
	push @output, "	rcp $tmp.z, $tmp.y																								; LINEINFO(SDK_macros.vsh)(467)\n";
	push @output, "	; Now we have d/h																								; LINEINFO(SDK_macros.vsh)(469)\n";
	push @output, "	mul $tmp.w, $projPos.w, $tmp.z																								; LINEINFO(SDK_macros.vsh)(470)\n";
	push @output, "	; We finally have d * hw / h																								; LINEINFO(SDK_macros.vsh)(472)\n";
	push @output, "	; $tmp.w is now the distance that we see through water.																								; LINEINFO(SDK_macros.vsh)(473)\n";
	push @output, "	mul $tmp.w, $tmp.x, $tmp.w																								; LINEINFO(SDK_macros.vsh)(474)\n";
	if( $g_dx9 )																								
	{																								
		push @output, "		mad $tmp, -$tmp.w, $cOOFogRange, $cFogOne																								; LINEINFO(SDK_macros.vsh)(478)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(479)\n";
		push @output, "		max $dest, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(480)\n";
	}																								
	else																								
	{																								
		push @output, "		mad $tmp, -$tmp.w, $cOOFogRange, $cFogOne																								; LINEINFO(SDK_macros.vsh)(484)\n";
		push @output, "		min $tmp, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(485)\n";
		push @output, "		max $dest.x, $tmp.x, $cZero																								; LINEINFO(SDK_macros.vsh)(486)\n";
	}																								
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_macros.vsh)(493)\n";
push @output, "; Main fogging routine																								; LINEINFO(SDK_macros.vsh)(494)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_macros.vsh)(495)\n";
sub CalcFog																								
{																								
	if( !defined $DOWATERFOG )																								
	{																								
		die "CalcFog called without using \$DOWATERFOG\n";																								
	}																								
	my $fogType;																								
	if( $DOWATERFOG == 0 )																								
	{																								
		$fogType = "rangefog";																										
	}																								
	else																								
	{																								
		$fogType = "heightfog";																								
	}																								
#	print "\$fogType = $fogType\n";																								
	push @output, "	; CalcFog																								; LINEINFO(SDK_macros.vsh)(514)\n";
	local( $worldPos ) = shift;																								
	local( $projPos ) = shift;																								
	local( $dest ) = shift;																								
	if ( $dest eq "" )																								
	{																								
		$dest = "oFog";																								
	}																								
	if( $fogType eq "rangefog" )																								
	{																								
		&DepthFog( $projPos, $dest );																								
	}																								
	elsif( $fogType eq "heightfog" )																								
	{																								
		&WaterDepthFog( $worldPos, $projPos, $dest );																								
	}																								
	else																								
	{																								
		die;																								
	}																									
}																								
sub CalcRangeFog																								
{																								
	push @output, "	; CalcFog																								; LINEINFO(SDK_macros.vsh)(540)\n";
	local( $worldPos ) = shift;																								
	local( $projPos ) = shift;																								
	if( $DOWATERFOG == 0 )																								
	{																								
		&RangeFog( $projPos );																								
	}																								
	elsif( $DOWATERFOG == 1 )																								
	{																								
		&WaterRangeFog( $worldPos, $projPos );																								
	}																								
	else																								
	{																								
		die;																								
	}																									
}																								
sub GammaToLinear																								
{																								
	local( $gamma ) = shift;																								
	local( $linear ) = shift;																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; Is rcp more expensive than just storing 2.2 somewhere and doing a mov?																								; LINEINFO(SDK_macros.vsh)(566)\n";
	push @output, "	rcp $gamma.w, $cOOGamma							; $gamma.w = 2.2																								; LINEINFO(SDK_macros.vsh)(567)\n";
	push @output, "	lit $linear.z, $gamma.zzzw						; r0.z = linear blue																								; LINEINFO(SDK_macros.vsh)(568)\n";
	push @output, "	lit $tmp.z, $gamma.yyyw							; r2.z = linear green																								; LINEINFO(SDK_macros.vsh)(569)\n";
	push @output, "	mov $linear.y, $tmp.z							; r0.y = linear green																								; LINEINFO(SDK_macros.vsh)(570)\n";
	push @output, "	lit $tmp.z, $gamma.xxxw							; r2.z = linear red																								; LINEINFO(SDK_macros.vsh)(571)\n";
	push @output, "	mov $linear.x, $tmp.z							; r0.x = linear red																								; LINEINFO(SDK_macros.vsh)(572)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub LinearToGamma																								
{																								
	local( $linear ) = shift;																								
	local( $gamma ) = shift;																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	mov $linear.w, $cOOGamma						; $linear.w = 1.0/2.2																								; LINEINFO(SDK_macros.vsh)(585)\n";
	push @output, "	lit $gamma.z, $linear.zzzw						; r0.z = gamma blue																								; LINEINFO(SDK_macros.vsh)(586)\n";
	push @output, "	lit $tmp.z, $linear.yyyw						; r2.z = gamma green																								; LINEINFO(SDK_macros.vsh)(587)\n";
	push @output, "	mov $gamma.y, $tmp.z							; r0.y = gamma green																								; LINEINFO(SDK_macros.vsh)(588)\n";
	push @output, "	lit $tmp.z, $linear.xxxw						; r2.z = gamma red																								; LINEINFO(SDK_macros.vsh)(589)\n";
	push @output, "	mov $gamma.x, $tmp.z							; r0.x = gamma red																								; LINEINFO(SDK_macros.vsh)(590)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub ComputeReflectionVector																								
{																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	local( $reflectionVector ) = shift;																								
	local( $vertToEye ); &AllocateRegister( \$vertToEye, "\$vertToEye" );																								
	local( $tmp ); &AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; compute reflection vector r = 2 * (n dot v) n - v																								; LINEINFO(SDK_macros.vsh)(604)\n";
	push @output, "	sub $vertToEye.xyz, $cEyePos.xyz, $worldPos  ; $tmp1 = v = c - p																								; LINEINFO(SDK_macros.vsh)(605)\n";
	push @output, "	dp3 $tmp, $worldNormal, $vertToEye			; $tmp = n dot v																								; LINEINFO(SDK_macros.vsh)(606)\n";
	push @output, "	mul $tmp.xyz, $tmp.xyz, $worldNormal	; $tmp = (n dot v ) n																								; LINEINFO(SDK_macros.vsh)(607)\n";
	push @output, "	mad $reflectionVector.xyz, $tmp, $cTwo, -$vertToEye																								; LINEINFO(SDK_macros.vsh)(608)\n";
	&FreeRegister( \$vertToEye, "\$vertToEye" );																								
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub ComputeSphereMapTexCoords																								
{																								
	local( $reflectionVector ) = shift;																								
	local( $sphereMapTexCoords ) = shift;																								
	local( $tmp ); &AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; transform reflection vector into view space																								; LINEINFO(SDK_macros.vsh)(621)\n";
	push @output, "	dp3 $tmp.x, $reflectionVector, $cViewModel0																								; LINEINFO(SDK_macros.vsh)(622)\n";
	push @output, "	dp3 $tmp.y, $reflectionVector, $cViewModel1																								; LINEINFO(SDK_macros.vsh)(623)\n";
	push @output, "	dp3 $tmp.z, $reflectionVector, $cViewModel2																								; LINEINFO(SDK_macros.vsh)(624)\n";
	push @output, "	; generate <rx ry rz+1>																								; LINEINFO(SDK_macros.vsh)(626)\n";
	push @output, "	add $tmp.z, $tmp.z, $cOne																								; LINEINFO(SDK_macros.vsh)(627)\n";
	push @output, "	; find 1 / the length of r2																								; LINEINFO(SDK_macros.vsh)(629)\n";
	push @output, "	dp3 $tmp.w, $tmp, $tmp																								; LINEINFO(SDK_macros.vsh)(630)\n";
	push @output, "	rsq $tmp.w, $tmp.w																								; LINEINFO(SDK_macros.vsh)(631)\n";
	push @output, "	; r1 = r2/|r2| + 1																								; LINEINFO(SDK_macros.vsh)(633)\n";
	push @output, "	mad $tmp.xy, $tmp.w, $tmp, $cOne																								; LINEINFO(SDK_macros.vsh)(634)\n";
	push @output, "	mul $sphereMapTexCoords.xy, $tmp.xy, $cHalf																								; LINEINFO(SDK_macros.vsh)(635)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub FixupXboxBoneIndex_1Bone																								
{																								
	local( $boneIndices ) = shift;																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; $boneIndices.z = $boneIndices.z - ( $boneIndices.z >= 96 ? 192 : 0 )																								; LINEINFO(SDK_macros.vsh)(647)\n";
	push @output, "	add $tmp.x, $cModel0Index, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(648)\n";
	push @output, "	sge $tmp.y, $boneIndices.z, $tmp.x																								; LINEINFO(SDK_macros.vsh)(649)\n";
	push @output, "	mul $tmp.y, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(650)\n";
	push @output, "	mad $boneIndices.z, $tmp.y, -$cTwo, $boneIndices.z 																								; LINEINFO(SDK_macros.vsh)(651)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub FixupXboxBoneIndex_2Bone																								
{																								
	local( $boneIndices ) = shift;																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; $boneIndices.z = $boneIndices.z - ( $boneIndices.z >= 96 ? 192 : 0 )																								; LINEINFO(SDK_macros.vsh)(663)\n";
	push @output, "	add $tmp.x, $cModel0Index, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(664)\n";
	push @output, "	sge $tmp.y, $boneIndices.z, $tmp.x																								; LINEINFO(SDK_macros.vsh)(665)\n";
	push @output, "	mul $tmp.y, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(666)\n";
	push @output, "	mad $boneIndices.z, $tmp.y, -$cTwo, $boneIndices.z 																								; LINEINFO(SDK_macros.vsh)(667)\n";
	push @output, "	; $boneIndices.y = $boneIndices.y - ( $boneIndices.y >= 96 ? 192 : 0 )																								; LINEINFO(SDK_macros.vsh)(669)\n";
	push @output, "	sge $tmp.y, $boneIndices.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(670)\n";
	push @output, "	mul $tmp.y, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(671)\n";
	push @output, "	mad $boneIndices.y, $tmp.y, -$cTwo, $boneIndices.y 																								; LINEINFO(SDK_macros.vsh)(672)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub FixupXboxBoneIndex_3Bone																								
{																								
	local( $boneIndices ) = shift;																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" );																								
	push @output, "	; $boneIndices.z = $boneIndices.z - ( $boneIndices.z >= 96 ? 192 : 0 )																								; LINEINFO(SDK_macros.vsh)(684)\n";
	push @output, "	add $tmp.x, $cModel0Index, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(685)\n";
	push @output, "	sge $tmp.y, $boneIndices.z, $tmp.x																								; LINEINFO(SDK_macros.vsh)(686)\n";
	push @output, "	mul $tmp.y, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(687)\n";
	push @output, "	mad $boneIndices.z, $tmp.y, -$cTwo, $boneIndices.z 																								; LINEINFO(SDK_macros.vsh)(688)\n";
	push @output, "	; $boneIndices.y = $boneIndices.y - ( $boneIndices.y >= 96 ? 192 : 0 )																								; LINEINFO(SDK_macros.vsh)(690)\n";
	push @output, "	sge $tmp.y, $boneIndices.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(691)\n";
	push @output, "	mul $tmp.y, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(692)\n";
	push @output, "	mad $boneIndices.y, $tmp.y, -$cTwo, $boneIndices.y 																								; LINEINFO(SDK_macros.vsh)(693)\n";
	push @output, "	; $boneIndices.x = $boneIndices.x - ( $boneIndices.x >= 96 ? 192 : 0 )																								; LINEINFO(SDK_macros.vsh)(695)\n";
	push @output, "	sge $tmp.y, $boneIndices.x, $tmp.x																								; LINEINFO(SDK_macros.vsh)(696)\n";
	push @output, "	mul $tmp.y, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(697)\n";
	push @output, "	mad $boneIndices.x, $tmp.y, -$cTwo, $boneIndices.x 																								; LINEINFO(SDK_macros.vsh)(698)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
}																								
sub SkinPosition																								
{																								
#	print "\$NUM_BONES = $NUM_BONES\n";																								
	local( $worldPos ) = shift;																								
	if( !defined $NUM_BONES && !defined $SKINNING )																								
	{																								
		die "using \$NUM_BONES or \$SKINNING without defining.\n";																								
	}																								
	if ( defined $SKINNING )																								
	{																								
		if( $SKINNING == 0 )																								
		{																								
			$NUM_BONES = 0;																								
		}																								
		else																								
		{																								
			$NUM_BONES = 3;																								
		}																								
	}																								
	if( $NUM_BONES == 0 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(726)\n";
		push @output, "		; 0 bone skinning (4 instructions)																								; LINEINFO(SDK_macros.vsh)(727)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(728)\n";
		push @output, "		; Transform position into world space																								; LINEINFO(SDK_macros.vsh)(729)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(730)\n";
		push @output, "		dp4 $worldPos.x, $vPos, $cModel0																								; LINEINFO(SDK_macros.vsh)(731)\n";
		push @output, "		dp4 $worldPos.y, $vPos, $cModel1																								; LINEINFO(SDK_macros.vsh)(732)\n";
		push @output, "		dp4 $worldPos.z, $vPos, $cModel2																								; LINEINFO(SDK_macros.vsh)(733)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(734)\n";
	} 																								
	elsif( $NUM_BONES == 1 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(738)\n";
		push @output, "		; 1 bone skinning (6 instructions)																								; LINEINFO(SDK_macros.vsh)(739)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(740)\n";
		local( $boneIndices );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		push @output, "		; Perform 1 bone skinning																								; LINEINFO(SDK_macros.vsh)(745)\n";
		push @output, "		; Transform position into world space																								; LINEINFO(SDK_macros.vsh)(746)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(747)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(748)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_1Bone( $boneIndices );																								
		}																								
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(753)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(755)\n";
		push @output, "		dp4 $worldPos.x, $vPos, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(756)\n";
		push @output, "		dp4 $worldPos.y, $vPos, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(757)\n";
		push @output, "		dp4 $worldPos.z, $vPos, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(758)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(759)\n";
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
	}																								
	elsif( $NUM_BONES == 2 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(765)\n";
		push @output, "		; 2 bone skinning (13 instructions)																								; LINEINFO(SDK_macros.vsh)(766)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(767)\n";
		local( $boneIndices );																								
		local( $blendedMatrix0 );																								
		local( $blendedMatrix1 );																								
		local( $blendedMatrix2 );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		&AllocateRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&AllocateRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&AllocateRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
		push @output, "		; Transform position into world space using all bones																								; LINEINFO(SDK_macros.vsh)(778)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(779)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(780)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_2Bone( $boneIndices );																								
		}																								
		push @output, "		; r11 = boneindices at this point																								; LINEINFO(SDK_macros.vsh)(786)\n";
		push @output, "		; first matrix																								; LINEINFO(SDK_macros.vsh)(787)\n";
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(788)\n";
		push @output, "		mul $blendedMatrix0, $vBoneWeights.x, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(789)\n";
		push @output, "		mul $blendedMatrix1, $vBoneWeights.x, c[a0.x+1]																								; LINEINFO(SDK_macros.vsh)(790)\n";
		push @output, "		mul $blendedMatrix2, $vBoneWeights.x, c[a0.x+2]																								; LINEINFO(SDK_macros.vsh)(791)\n";
		push @output, "		; second matrix																								; LINEINFO(SDK_macros.vsh)(792)\n";
		push @output, "		mov a0.x, $boneIndices.y																								; LINEINFO(SDK_macros.vsh)(793)\n";
		push @output, "		mad $blendedMatrix0, $vBoneWeights.y, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(794)\n";
		push @output, "		mad $blendedMatrix1, $vBoneWeights.y, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(795)\n";
		push @output, "		mad $blendedMatrix2, $vBoneWeights.y, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(796)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(798)\n";
		push @output, "		dp4 $worldPos.x, $vPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(799)\n";
		push @output, "		dp4 $worldPos.y, $vPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(800)\n";
		push @output, "		dp4 $worldPos.z, $vPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(801)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(802)\n";
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
		&FreeRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&FreeRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&FreeRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
	}																								
	elsif( $NUM_BONES == 3 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(811)\n";
		push @output, "		; 3 bone skinning  (19 instructions)																								; LINEINFO(SDK_macros.vsh)(812)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(813)\n";
		local( $boneIndices );																								
		local( $blendedMatrix0 );																								
		local( $blendedMatrix1 );																								
		local( $blendedMatrix2 );																								
		local( $localPos );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		&AllocateRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&AllocateRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&AllocateRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
		push @output, "		; Transform position into world space using all bones																								; LINEINFO(SDK_macros.vsh)(824)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(825)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(826)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_3Bone( $boneIndices );																								
			&AllocateRegister( \$localPos, "\$localPos" );																								
			push @output, "			mov $localPos, $vPos																								; LINEINFO(SDK_macros.vsh)(831)\n";
			push @output, "			mad $localPos.xyz, $SHADER_FLEXSCALE, $vPosFlex.xyz, $localPos.xyz																								; LINEINFO(SDK_macros.vsh)(832)\n";
		}																								
		push @output, "		; r11 = boneindices at this point																								; LINEINFO(SDK_macros.vsh)(835)\n";
		push @output, "		; first matrix																								; LINEINFO(SDK_macros.vsh)(836)\n";
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(837)\n";
		push @output, "		mul $blendedMatrix0, $vBoneWeights.x, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(838)\n";
		push @output, "		mul $blendedMatrix1, $vBoneWeights.x, c[a0.x+1]																								; LINEINFO(SDK_macros.vsh)(839)\n";
		push @output, "		mul $blendedMatrix2, $vBoneWeights.x, c[a0.x+2]																								; LINEINFO(SDK_macros.vsh)(840)\n";
		push @output, "		; second matrix																								; LINEINFO(SDK_macros.vsh)(841)\n";
		push @output, "		mov a0.x, $boneIndices.y																								; LINEINFO(SDK_macros.vsh)(842)\n";
		push @output, "		mad $blendedMatrix0, $vBoneWeights.y, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(843)\n";
		push @output, "		mad $blendedMatrix1, $vBoneWeights.y, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(844)\n";
		push @output, "		mad $blendedMatrix2, $vBoneWeights.y, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(845)\n";
		push @output, "		; Calculate third weight																								; LINEINFO(SDK_macros.vsh)(847)\n";
		push @output, "		; compute 1-(weight1+weight2) to calculate weight2																								; LINEINFO(SDK_macros.vsh)(848)\n";
		push @output, "		; Use $boneIndices.w as a temp since we aren't using it for anything.																								; LINEINFO(SDK_macros.vsh)(849)\n";
		push @output, "		add $boneIndices.w, $vBoneWeights.x, $vBoneWeights.y																								; LINEINFO(SDK_macros.vsh)(850)\n";
		push @output, "		sub $boneIndices.w, $cOne, $boneIndices.w																								; LINEINFO(SDK_macros.vsh)(851)\n";
		push @output, "		; third matrix																								; LINEINFO(SDK_macros.vsh)(853)\n";
		push @output, "		mov a0.x, $boneIndices.x																								; LINEINFO(SDK_macros.vsh)(854)\n";
		push @output, "		mad $blendedMatrix0, $boneIndices.w, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(855)\n";
		push @output, "		mad $blendedMatrix1, $boneIndices.w, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(856)\n";
		push @output, "		mad $blendedMatrix2, $boneIndices.w, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(857)\n";
		if ( $g_xbox )																								
		{																								
			push @output, "			dp4 $worldPos.x, $localPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(861)\n";
			push @output, "			dp4 $worldPos.y, $localPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(862)\n";
			push @output, "			dp4 $worldPos.z, $localPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(863)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(864)\n";
			&FreeRegister( \$localPos, "\$localPos" );																								
		}																								
		else																								
		{																								
			push @output, "			dp4 $worldPos.x, $vPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(869)\n";
			push @output, "			dp4 $worldPos.y, $vPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(870)\n";
			push @output, "			dp4 $worldPos.z, $vPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(871)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(872)\n";
		}																								
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
		&FreeRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&FreeRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&FreeRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
	}																								
}																								
sub SkinPositionAndNormal																								
{																								
#	print "\$NUM_BONES = $NUM_BONES\n";																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	if( !defined $NUM_BONES && !defined $SKINNING )																								
	{																								
		die "using \$NUM_BONES or \$SKINNING without defining.\n";																								
	}																								
	if ( defined $SKINNING )																								
	{																								
		if( $SKINNING == 0 )																								
		{																								
			$NUM_BONES = 0;																								
		}																								
		else																								
		{																								
			$NUM_BONES = 3;																								
		}																								
	}																								
	if( $NUM_BONES == 0 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(907)\n";
		push @output, "		; 0 bone skinning (13 instructions)																								; LINEINFO(SDK_macros.vsh)(908)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(909)\n";
		push @output, "		; Transform position + normal + tangentS + tangentT into world space																								; LINEINFO(SDK_macros.vsh)(910)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(911)\n";
		push @output, "		dp4 $worldPos.x, $vPos, $cModel0																								; LINEINFO(SDK_macros.vsh)(912)\n";
		push @output, "		dp4 $worldPos.y, $vPos, $cModel1																								; LINEINFO(SDK_macros.vsh)(913)\n";
		push @output, "		dp4 $worldPos.z, $vPos, $cModel2																								; LINEINFO(SDK_macros.vsh)(914)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(915)\n";
		push @output, "		; normal																								; LINEINFO(SDK_macros.vsh)(916)\n";
		push @output, "		dp3 $worldNormal.x, $vNormal, $cModel0																								; LINEINFO(SDK_macros.vsh)(917)\n";
		push @output, "		dp3 $worldNormal.y, $vNormal, $cModel1																								; LINEINFO(SDK_macros.vsh)(918)\n";
		push @output, "		dp3 $worldNormal.z, $vNormal, $cModel2																								; LINEINFO(SDK_macros.vsh)(919)\n";
	}																								
	elsif( $NUM_BONES == 1 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(923)\n";
		push @output, "		; 1 bone skinning (17 instructions)																								; LINEINFO(SDK_macros.vsh)(924)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(925)\n";
		local( $boneIndices );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		push @output, "		; Perform 1 bone skinning																								; LINEINFO(SDK_macros.vsh)(930)\n";
		push @output, "		; Transform position into world space																								; LINEINFO(SDK_macros.vsh)(931)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(932)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(933)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_1Bone( $boneIndices );																								
		}																								
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(938)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(940)\n";
		push @output, "		dp4 $worldPos.x, $vPos, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(941)\n";
		push @output, "		dp4 $worldPos.y, $vPos, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(942)\n";
		push @output, "		dp4 $worldPos.z, $vPos, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(943)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(944)\n";
		push @output, "		; normal																								; LINEINFO(SDK_macros.vsh)(946)\n";
		push @output, "		dp3 $worldNormal.x, $vNormal, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(947)\n";
		push @output, "		dp3 $worldNormal.y, $vNormal, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(948)\n";
		push @output, "		dp3 $worldNormal.z, $vNormal, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(949)\n";
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
	}																								
	elsif( $NUM_BONES == 2 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(955)\n";
		push @output, "		; 2 bone skinning (16 instructions)																								; LINEINFO(SDK_macros.vsh)(956)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(957)\n";
		local( $boneIndices );																								
		local( $blendedMatrix0 );																								
		local( $blendedMatrix1 );																								
		local( $blendedMatrix2 );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		&AllocateRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&AllocateRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&AllocateRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
		push @output, "		; Transform position into world space using all bones																								; LINEINFO(SDK_macros.vsh)(968)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(969)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(970)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_2Bone( $boneIndices );																								
		}																								
		push @output, "		; r11 = boneindices at this point																								; LINEINFO(SDK_macros.vsh)(975)\n";
		push @output, "		; first matrix																								; LINEINFO(SDK_macros.vsh)(976)\n";
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(977)\n";
		push @output, "		mul $blendedMatrix0, $vBoneWeights.x, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(978)\n";
		push @output, "		mul $blendedMatrix1, $vBoneWeights.x, c[a0.x+1]																								; LINEINFO(SDK_macros.vsh)(979)\n";
		push @output, "		mul $blendedMatrix2, $vBoneWeights.x, c[a0.x+2]																								; LINEINFO(SDK_macros.vsh)(980)\n";
		push @output, "		; second matrix																								; LINEINFO(SDK_macros.vsh)(981)\n";
		push @output, "		mov a0.x, $boneIndices.y																								; LINEINFO(SDK_macros.vsh)(982)\n";
		push @output, "		mad $blendedMatrix0, $vBoneWeights.y, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(983)\n";
		push @output, "		mad $blendedMatrix1, $vBoneWeights.y, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(984)\n";
		push @output, "		mad $blendedMatrix2, $vBoneWeights.y, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(985)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(987)\n";
		push @output, "		dp4 $worldPos.x, $vPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(988)\n";
		push @output, "		dp4 $worldPos.y, $vPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(989)\n";
		push @output, "		dp4 $worldPos.z, $vPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(990)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(991)\n";
		push @output, "		; normal																								; LINEINFO(SDK_macros.vsh)(993)\n";
		push @output, "		dp3 $worldNormal.x, $vNormal, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(994)\n";
		push @output, "		dp3 $worldNormal.y, $vNormal, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(995)\n";
		push @output, "		dp3 $worldNormal.z, $vNormal, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(996)\n";
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
		&FreeRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&FreeRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&FreeRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
	}																								
	elsif( $NUM_BONES == 3 )																								
	{																								
		local( $boneIndices );																								
		local( $blendedMatrix0 );																								
		local( $blendedMatrix1 );																								
		local( $blendedMatrix2 );																								
		local( $localPos );																								
		local( $localNormal );																								
		local( $normalLength );																								
		local( $ooNormalLength );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		&AllocateRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&AllocateRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&AllocateRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
		push @output, "		; Transform position into world space using all bones																								; LINEINFO(SDK_macros.vsh)(1018)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(1019)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(1020)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_3Bone( $boneIndices );																								
			&AllocateRegister( \$localPos, "\$localPos" );																								
			push @output, "			mov $localPos, $vPos																								; LINEINFO(SDK_macros.vsh)(1025)\n";
			push @output, "			mad $localPos.xyz, $SHADER_FLEXSCALE, $vPosFlex.xyz, $localPos.xyz																								; LINEINFO(SDK_macros.vsh)(1026)\n";
			&AllocateRegister( \$localNormal, "\$localNormal" );																								
			push @output, "			mov $localNormal, $vNormal																								; LINEINFO(SDK_macros.vsh)(1029)\n";
			push @output, "			mad $localNormal.xyz, $SHADER_FLEXSCALE, $vNormalFlex.xyz, $localNormal.xyz																								; LINEINFO(SDK_macros.vsh)(1030)\n";
		}																								
		push @output, "		; r11 = boneindices at this point																								; LINEINFO(SDK_macros.vsh)(1032)\n";
		push @output, "		; first matrix																								; LINEINFO(SDK_macros.vsh)(1033)\n";
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(1034)\n";
		push @output, "		mul $blendedMatrix0, $vBoneWeights.x, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1035)\n";
		push @output, "		mul $blendedMatrix1, $vBoneWeights.x, c[a0.x+1]																								; LINEINFO(SDK_macros.vsh)(1036)\n";
		push @output, "		mul $blendedMatrix2, $vBoneWeights.x, c[a0.x+2]																								; LINEINFO(SDK_macros.vsh)(1037)\n";
		push @output, "		; second matrix																								; LINEINFO(SDK_macros.vsh)(1038)\n";
		push @output, "		mov a0.x, $boneIndices.y																								; LINEINFO(SDK_macros.vsh)(1039)\n";
		push @output, "		mad $blendedMatrix0, $vBoneWeights.y, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1040)\n";
		push @output, "		mad $blendedMatrix1, $vBoneWeights.y, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1041)\n";
		push @output, "		mad $blendedMatrix2, $vBoneWeights.y, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1042)\n";
		push @output, "		; Calculate third weight																								; LINEINFO(SDK_macros.vsh)(1044)\n";
		push @output, "		; compute 1-(weight1+weight2) to calculate weight2																								; LINEINFO(SDK_macros.vsh)(1045)\n";
		push @output, "		; Use $boneIndices.w as a temp since we aren't using it for anything.																								; LINEINFO(SDK_macros.vsh)(1046)\n";
		push @output, "		add $boneIndices.w, $vBoneWeights.x, $vBoneWeights.y																								; LINEINFO(SDK_macros.vsh)(1047)\n";
		push @output, "		sub $boneIndices.w, $cOne, $boneIndices.w																								; LINEINFO(SDK_macros.vsh)(1048)\n";
		push @output, "		; third matrix																								; LINEINFO(SDK_macros.vsh)(1050)\n";
		push @output, "		mov a0.x, $boneIndices.x																								; LINEINFO(SDK_macros.vsh)(1051)\n";
		push @output, "		mad $blendedMatrix0, $boneIndices.w, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1052)\n";
		push @output, "		mad $blendedMatrix1, $boneIndices.w, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1053)\n";
		push @output, "		mad $blendedMatrix2, $boneIndices.w, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1054)\n";
		if ( $g_xbox )																								
		{																								
			push @output, "			dp4 $worldPos.x, $localPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1058)\n";
			push @output, "			dp4 $worldPos.y, $localPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1059)\n";
			push @output, "			dp4 $worldPos.z, $localPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1060)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1061)\n";
			&FreeRegister( \$localPos, "\$localPos" );																								
			push @output, "			; normal																								; LINEINFO(SDK_macros.vsh)(1064)\n";
			push @output, "			dp3 $worldNormal.x, $localNormal, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1065)\n";
			push @output, "			dp3 $worldNormal.y, $localNormal, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1066)\n";
			push @output, "			dp3 $worldNormal.z, $localNormal, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1067)\n";
			push @output, "			; renormalize after flex																								; LINEINFO(SDK_macros.vsh)(1069)\n";
			&FreeRegister( \$localNormal, "\$localNormal" );																								
			&AllocateRegister( \$normalLength, "\$normalLength" );																								
			&AllocateRegister( \$ooNormalLength, "\$ooNormalLength" );																								
			push @output, "			dp3 $normalLength, $worldNormal, $worldNormal																								; LINEINFO(SDK_macros.vsh)(1074)\n";
			push @output, "			rsq $ooNormalLength.x, $normalLength.x																								; LINEINFO(SDK_macros.vsh)(1075)\n";
			push @output, "			mul $worldNormal.xyz, $worldNormal.xyz, $ooNormalLength.x																								; LINEINFO(SDK_macros.vsh)(1076)\n";
			&FreeRegister( \$normalLength, "\$normalLength" );																								
			&FreeRegister( \$ooNormalLength, "\$ooNormalLength" );																								
		}																								
		else																								
		{																								
			push @output, "			dp4 $worldPos.x, $vPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1083)\n";
			push @output, "			dp4 $worldPos.y, $vPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1084)\n";
			push @output, "			dp4 $worldPos.z, $vPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1085)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1086)\n";
			push @output, "			; normal																								; LINEINFO(SDK_macros.vsh)(1088)\n";
			push @output, "			dp3 $worldNormal.x, $vNormal, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1089)\n";
			push @output, "			dp3 $worldNormal.y, $vNormal, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1090)\n";
			push @output, "			dp3 $worldNormal.z, $vNormal, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1091)\n";
		}																								
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
		&FreeRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&FreeRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&FreeRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
	}																									
}																								
sub SkinPositionNormalAndTangentSpace																								
{																								
#	print "\$NUM_BONES = $NUM_BONES\n";																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	local( $worldTangentS ) = shift;																								
	local( $worldTangentT ) = shift;																								
	local( $userData );																								
	local( $localPos );																								
	local( $localNormal );																								
	local( $normalLength );																								
	local( $ooNormalLength );																								
	if( !defined $NUM_BONES && !defined $SKINNING )																								
	{																								
		die "using \$NUM_BONES or \$SKINNING without defining.\n";																								
	}																								
	if ( defined $SKINNING )																								
	{																								
		if( $SKINNING == 0 )																								
		{																								
			$NUM_BONES = 0;																								
		}																								
		else																								
		{																								
			$NUM_BONES = 3;																								
		}																								
	}																								
	if ( $g_xbox )																								
	{																								
		&AllocateRegister( \$userData, "\$userData" );																								
		push @output, "		; remap compressed range [0..1] to [-1..1]																								; LINEINFO(SDK_macros.vsh)(1132)\n";
		push @output, "		mad $userData, $vUserData, $cTwo, -$cOne																								; LINEINFO(SDK_macros.vsh)(1133)\n";
	}																								
	if( $NUM_BONES == 0 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(1138)\n";
		push @output, "		; 0 bone skinning (13 instructions)																								; LINEINFO(SDK_macros.vsh)(1139)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(1140)\n";
		push @output, "		; Transform position + normal + tangentS + tangentT into world space																								; LINEINFO(SDK_macros.vsh)(1141)\n";
		if ( $g_xbox )																								
		{																								
			&AllocateRegister( \$localPos, "\$localPos" );																								
			push @output, "			mov $localPos, $vPos																								; LINEINFO(SDK_macros.vsh)(1146)\n";
			push @output, "			mad $localPos.xyz, $SHADER_FLEXSCALE, $vPosFlex.xyz, $localPos.xyz																								; LINEINFO(SDK_macros.vsh)(1147)\n";
			push @output, "			dp4 $worldPos.x, $localPos, $cModel0																								; LINEINFO(SDK_macros.vsh)(1148)\n";
			push @output, "			dp4 $worldPos.y, $localPos, $cModel1																								; LINEINFO(SDK_macros.vsh)(1149)\n";
			push @output, "			dp4 $worldPos.z, $localPos, $cModel2																								; LINEINFO(SDK_macros.vsh)(1150)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1151)\n";
			&FreeRegister( \$localPos, "\$localPos" );																								
		}																								
		else																								
		{																								
			push @output, "			dp4 $worldPos.x, $vPos, $cModel0																								; LINEINFO(SDK_macros.vsh)(1156)\n";
			push @output, "			dp4 $worldPos.y, $vPos, $cModel1																								; LINEINFO(SDK_macros.vsh)(1157)\n";
			push @output, "			dp4 $worldPos.z, $vPos, $cModel2																								; LINEINFO(SDK_macros.vsh)(1158)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1159)\n";
		}																								
		push @output, "		; normal																								; LINEINFO(SDK_macros.vsh)(1162)\n";
		push @output, "		dp3 $worldNormal.x, $vNormal, $cModel0																								; LINEINFO(SDK_macros.vsh)(1163)\n";
		push @output, "		dp3 $worldNormal.y, $vNormal, $cModel1																								; LINEINFO(SDK_macros.vsh)(1164)\n";
		push @output, "		dp3 $worldNormal.z, $vNormal, $cModel2																								; LINEINFO(SDK_macros.vsh)(1165)\n";
		if ( $g_xbox )																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1169)\n";
			push @output, "			dp3 $worldTangentS.x, $userData, $cModel0																								; LINEINFO(SDK_macros.vsh)(1170)\n";
			push @output, "			dp3 $worldTangentS.y, $userData, $cModel1																								; LINEINFO(SDK_macros.vsh)(1171)\n";
			push @output, "			dp3 $worldTangentS.z, $userData, $cModel2																								; LINEINFO(SDK_macros.vsh)(1172)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1174)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $userData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1176)\n";
		}																								
		else																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1180)\n";
			push @output, "			dp3 $worldTangentS.x, $vUserData, $cModel0																								; LINEINFO(SDK_macros.vsh)(1181)\n";
			push @output, "			dp3 $worldTangentS.y, $vUserData, $cModel1																								; LINEINFO(SDK_macros.vsh)(1182)\n";
			push @output, "			dp3 $worldTangentS.z, $vUserData, $cModel2																								; LINEINFO(SDK_macros.vsh)(1183)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1185)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $vUserData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1187)\n";
		}																								
	}																								
	elsif( $NUM_BONES == 1 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(1192)\n";
		push @output, "		; 1 bone skinning (17 instructions)																								; LINEINFO(SDK_macros.vsh)(1193)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(1194)\n";
		local( $boneIndices );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		push @output, "		; Perform 1 bone skinning																								; LINEINFO(SDK_macros.vsh)(1199)\n";
		push @output, "		; Transform position into world space																								; LINEINFO(SDK_macros.vsh)(1200)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(1201)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(1202)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_1Bone( $boneIndices );																								
		}																								
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(1207)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(1209)\n";
		push @output, "		dp4 $worldPos.x, $vPos, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1210)\n";
		push @output, "		dp4 $worldPos.y, $vPos, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(1211)\n";
		push @output, "		dp4 $worldPos.z, $vPos, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(1212)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1213)\n";
		push @output, "		; normal																								; LINEINFO(SDK_macros.vsh)(1215)\n";
		push @output, "		dp3 $worldNormal.x, $vNormal, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1216)\n";
		push @output, "		dp3 $worldNormal.y, $vNormal, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(1217)\n";
		push @output, "		dp3 $worldNormal.z, $vNormal, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(1218)\n";
		if ( $g_xbox )																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1222)\n";
			push @output, "			dp3 $worldTangentS.x, $userData, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1223)\n";
			push @output, "			dp3 $worldTangentS.y, $userData, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(1224)\n";
			push @output, "			dp3 $worldTangentS.z, $userData, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(1225)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1227)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $userData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1229)\n";
		}																								
		else																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1233)\n";
			push @output, "			dp3 $worldTangentS.x, $vUserData, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1234)\n";
			push @output, "			dp3 $worldTangentS.y, $vUserData, c[a0.x + 1]																								; LINEINFO(SDK_macros.vsh)(1235)\n";
			push @output, "			dp3 $worldTangentS.z, $vUserData, c[a0.x + 2]																								; LINEINFO(SDK_macros.vsh)(1236)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1238)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $vUserData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1240)\n";
		}																								
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
	}																								
	elsif( $NUM_BONES == 2 )																								
	{																								
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(1247)\n";
		push @output, "		; 2 bone skinning (22 instructions)																								; LINEINFO(SDK_macros.vsh)(1248)\n";
		push @output, "		;																								; LINEINFO(SDK_macros.vsh)(1249)\n";
		local( $boneIndices );																								
		local( $blendedMatrix0 );																								
		local( $blendedMatrix1 );																								
		local( $blendedMatrix2 );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		&AllocateRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&AllocateRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&AllocateRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
		push @output, "		; Transform position into world space using all bones																								; LINEINFO(SDK_macros.vsh)(1260)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(1261)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(1262)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_2Bone( $boneIndices );																								
		}																								
		push @output, "		; r11 = boneindices at this point																								; LINEINFO(SDK_macros.vsh)(1267)\n";
		push @output, "		; first matrix																								; LINEINFO(SDK_macros.vsh)(1268)\n";
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(1269)\n";
		push @output, "		mul $blendedMatrix0, $vBoneWeights.x, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1270)\n";
		push @output, "		mul $blendedMatrix1, $vBoneWeights.x, c[a0.x+1]																								; LINEINFO(SDK_macros.vsh)(1271)\n";
		push @output, "		mul $blendedMatrix2, $vBoneWeights.x, c[a0.x+2]																								; LINEINFO(SDK_macros.vsh)(1272)\n";
		push @output, "		; second matrix																								; LINEINFO(SDK_macros.vsh)(1273)\n";
		push @output, "		mov a0.x, $boneIndices.y																								; LINEINFO(SDK_macros.vsh)(1274)\n";
		push @output, "		mad $blendedMatrix0, $vBoneWeights.y, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1275)\n";
		push @output, "		mad $blendedMatrix1, $vBoneWeights.y, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1276)\n";
		push @output, "		mad $blendedMatrix2, $vBoneWeights.y, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1277)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(1279)\n";
		push @output, "		dp4 $worldPos.x, $vPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1280)\n";
		push @output, "		dp4 $worldPos.y, $vPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1281)\n";
		push @output, "		dp4 $worldPos.z, $vPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1282)\n";
		push @output, "		mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1283)\n";
		push @output, "		; normal																								; LINEINFO(SDK_macros.vsh)(1285)\n";
		push @output, "		dp3 $worldNormal.x, $vNormal, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1286)\n";
		push @output, "		dp3 $worldNormal.y, $vNormal, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1287)\n";
		push @output, "		dp3 $worldNormal.z, $vNormal, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1288)\n";
		if ( $g_xbox )																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1292)\n";
			push @output, "			dp3 $worldTangentS.x, $userData, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1293)\n";
			push @output, "			dp3 $worldTangentS.y, $userData, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1294)\n";
			push @output, "			dp3 $worldTangentS.z, $userData, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1295)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1297)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $userData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1299)\n";
		}																								
		else																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1303)\n";
			push @output, "			dp3 $worldTangentS.x, $vUserData, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1304)\n";
			push @output, "			dp3 $worldTangentS.y, $vUserData, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1305)\n";
			push @output, "			dp3 $worldTangentS.z, $vUserData, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1306)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1308)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $vUserData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1310)\n";
		}																								
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
		&FreeRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&FreeRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&FreeRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
	}																								
	elsif( $NUM_BONES == 3 )																								
	{																								
		local( $boneIndices );																								
		local( $blendedMatrix0 );																								
		local( $blendedMatrix1 );																								
		local( $blendedMatrix2 );																								
		&AllocateRegister( \$boneIndices, "\$boneIndices" );																								
		&AllocateRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&AllocateRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&AllocateRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
		if ( $g_xbox )																								
		{																								
			&AllocateRegister( \$localPos, "\$localPos" );																								
			push @output, "			mov $localPos, $vPos																								; LINEINFO(SDK_macros.vsh)(1331)\n";
			push @output, "			mad $localPos.xyz, $SHADER_FLEXSCALE, $vPosFlex.xyz, $localPos.xyz																								; LINEINFO(SDK_macros.vsh)(1332)\n";
			&AllocateRegister( \$localNormal, "\$localNormal" );																								
			push @output, "			mov $localNormal, $vNormal																								; LINEINFO(SDK_macros.vsh)(1335)\n";
			push @output, "			mad $localNormal.xyz, $SHADER_FLEXSCALE, $vNormalFlex.xyz, $localNormal.xyz																								; LINEINFO(SDK_macros.vsh)(1336)\n";
		}																								
		push @output, "		; Transform position into world space using all bones																								; LINEINFO(SDK_macros.vsh)(1339)\n";
		push @output, "		; denormalize d3dcolor to matrix index																								; LINEINFO(SDK_macros.vsh)(1340)\n";
		push @output, "		mad $boneIndices, $vBoneIndices, $cColorToIntScale, $cModel0Index																								; LINEINFO(SDK_macros.vsh)(1341)\n";
		if ( $g_xbox )																								
		{																								
			&FixupXboxBoneIndex_3Bone( $boneIndices );																								
		}																								
		push @output, "		; r11 = boneindices at this point																								; LINEINFO(SDK_macros.vsh)(1346)\n";
		push @output, "		; first matrix																								; LINEINFO(SDK_macros.vsh)(1347)\n";
		push @output, "		mov a0.x, $boneIndices.z																								; LINEINFO(SDK_macros.vsh)(1348)\n";
		push @output, "		mul $blendedMatrix0, $vBoneWeights.x, c[a0.x]																								; LINEINFO(SDK_macros.vsh)(1349)\n";
		push @output, "		mul $blendedMatrix1, $vBoneWeights.x, c[a0.x+1]																								; LINEINFO(SDK_macros.vsh)(1350)\n";
		push @output, "		mul $blendedMatrix2, $vBoneWeights.x, c[a0.x+2]																								; LINEINFO(SDK_macros.vsh)(1351)\n";
		push @output, "		; second matrix																								; LINEINFO(SDK_macros.vsh)(1352)\n";
		push @output, "		mov a0.x, $boneIndices.y																								; LINEINFO(SDK_macros.vsh)(1353)\n";
		push @output, "		mad $blendedMatrix0, $vBoneWeights.y, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1354)\n";
		push @output, "		mad $blendedMatrix1, $vBoneWeights.y, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1355)\n";
		push @output, "		mad $blendedMatrix2, $vBoneWeights.y, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1356)\n";
		push @output, "		; Calculate third weight																								; LINEINFO(SDK_macros.vsh)(1358)\n";
		push @output, "		; compute 1-(weight1+weight2) to calculate weight2																								; LINEINFO(SDK_macros.vsh)(1359)\n";
		push @output, "		; Use $boneIndices.w as a temp since we aren't using it for anything.																								; LINEINFO(SDK_macros.vsh)(1360)\n";
		push @output, "		add $boneIndices.w, $vBoneWeights.x, $vBoneWeights.y																								; LINEINFO(SDK_macros.vsh)(1361)\n";
		push @output, "		sub $boneIndices.w, $cOne, $boneIndices.w																								; LINEINFO(SDK_macros.vsh)(1362)\n";
		push @output, "		; third matrix																								; LINEINFO(SDK_macros.vsh)(1364)\n";
		push @output, "		mov a0.x, $boneIndices.x																								; LINEINFO(SDK_macros.vsh)(1365)\n";
		push @output, "		mad $blendedMatrix0, $boneIndices.w, c[a0.x], $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1366)\n";
		push @output, "		mad $blendedMatrix1, $boneIndices.w, c[a0.x+1], $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1367)\n";
		push @output, "		mad $blendedMatrix2, $boneIndices.w, c[a0.x+2], $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1368)\n";
		push @output, "		; position																								; LINEINFO(SDK_macros.vsh)(1370)\n";
		if ( $g_xbox )																								
		{																								
			push @output, "			dp4 $worldPos.x, $localPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1373)\n";
			push @output, "			dp4 $worldPos.y, $localPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1374)\n";
			push @output, "			dp4 $worldPos.z, $localPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1375)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1376)\n";
			&FreeRegister( \$localPos, "\$localPos" );																								
			push @output, "			; normal																								; LINEINFO(SDK_macros.vsh)(1379)\n";
			push @output, "			dp3 $worldNormal.x, $localNormal, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1380)\n";
			push @output, "			dp3 $worldNormal.y, $localNormal, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1381)\n";
			push @output, "			dp3 $worldNormal.z, $localNormal, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1382)\n";
			push @output, "			; renormalize after flex																								; LINEINFO(SDK_macros.vsh)(1384)\n";
			&FreeRegister( \$localNormal, "\$localNormal" );																								
			&AllocateRegister( \$normalLength, "\$normalLength" );																								
			&AllocateRegister( \$ooNormalLength, "\$ooNormalLength" );																								
			push @output, "			dp3 $normalLength, $worldNormal, $worldNormal																								; LINEINFO(SDK_macros.vsh)(1389)\n";
			push @output, "			rsq $ooNormalLength.x, $normalLength.x																								; LINEINFO(SDK_macros.vsh)(1390)\n";
			push @output, "			mul $worldNormal.xyz, $worldNormal.xyz, $ooNormalLength.x																								; LINEINFO(SDK_macros.vsh)(1391)\n";
			&FreeRegister( \$normalLength, "\$normalLength" );																								
			&FreeRegister( \$ooNormalLength, "\$ooNormalLength" );																								
		}																								
		else																								
		{																								
			push @output, "			dp4 $worldPos.x, $vPos, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1398)\n";
			push @output, "			dp4 $worldPos.y, $vPos, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1399)\n";
			push @output, "			dp4 $worldPos.z, $vPos, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1400)\n";
			push @output, "			mov $worldPos.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1401)\n";
			push @output, "			; normal																								; LINEINFO(SDK_macros.vsh)(1403)\n";
			push @output, "			dp3 $worldNormal.x, $vNormal, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1404)\n";
			push @output, "			dp3 $worldNormal.y, $vNormal, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1405)\n";
			push @output, "			dp3 $worldNormal.z, $vNormal, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1406)\n";
		}																								
		if ( $g_xbox )																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1411)\n";
			push @output, "			dp3 $worldTangentS.x, $userData, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1412)\n";
			push @output, "			dp3 $worldTangentS.y, $userData, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1413)\n";
			push @output, "			dp3 $worldTangentS.z, $userData, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1414)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1416)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $userData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1418)\n";
		}																								
		else																								
		{																								
			push @output, "			; tangents																								; LINEINFO(SDK_macros.vsh)(1422)\n";
			push @output, "			dp3 $worldTangentS.x, $vUserData, $blendedMatrix0																								; LINEINFO(SDK_macros.vsh)(1423)\n";
			push @output, "			dp3 $worldTangentS.y, $vUserData, $blendedMatrix1																								; LINEINFO(SDK_macros.vsh)(1424)\n";
			push @output, "			dp3 $worldTangentS.z, $vUserData, $blendedMatrix2																								; LINEINFO(SDK_macros.vsh)(1425)\n";
			push @output, "			; calculate tangent t via cross( N, S ) * S[3]																								; LINEINFO(SDK_macros.vsh)(1427)\n";
			&Cross( $worldTangentT, $worldNormal, $worldTangentS );																								
			push @output, "			mul $worldTangentT.xyz, $vUserData.w, $worldTangentT.xyz																								; LINEINFO(SDK_macros.vsh)(1429)\n";
		}																								
		&FreeRegister( \$boneIndices, "\$boneIndices" );																								
		&FreeRegister( \$blendedMatrix0, "\$blendedMatrix0" );																								
		&FreeRegister( \$blendedMatrix1, "\$blendedMatrix1" );																								
		&FreeRegister( \$blendedMatrix2, "\$blendedMatrix2" );																								
	}																								
	if ( $g_xbox )																								
	{																								
		&FreeRegister( \$userData, "\$userData" );																								
	}																								
}																								
sub ColorClamp																								
{																								
	push @output, "	; ColorClamp; stomps $color.w																								; LINEINFO(SDK_macros.vsh)(1446)\n";
	local( $color ) = shift;																								
	local( $dst ) = shift;																								
	push @output, "	; Get the max of RGB and stick it in W																								; LINEINFO(SDK_macros.vsh)(1450)\n";
	push @output, "	max $color.w, $color.x, $color.y																								; LINEINFO(SDK_macros.vsh)(1451)\n";
	push @output, "	max $color.w, $color.w, $color.z																								; LINEINFO(SDK_macros.vsh)(1452)\n";
	push @output, "	; get the greater of one and the max color.																								; LINEINFO(SDK_macros.vsh)(1454)\n";
	push @output, "	max $color.w, $color.w, $cOne																								; LINEINFO(SDK_macros.vsh)(1455)\n";
	push @output, "	rcp $color.w, $color.w																								; LINEINFO(SDK_macros.vsh)(1457)\n";
	push @output, "	mul $dst.xyz, $color.w, $color.xyz																								; LINEINFO(SDK_macros.vsh)(1458)\n";
}																								
sub AmbientLight																								
{																								
	local( $worldNormal ) = shift;																								
	local( $linearColor ) = shift;																								
	local( $add ) = shift;																								
	push @output, "	; Ambient lighting																								; LINEINFO(SDK_macros.vsh)(1467)\n";
	&AllocateRegister( \$nSquared, "\$nSquared" );																								
	&AllocateRegister( \$isNegative, "\$isNegative" );																								
	push @output, "	mul $nSquared.xyz, $worldNormal.xyz, $worldNormal.xyz				; compute n times n																								; LINEINFO(SDK_macros.vsh)(1471)\n";
	push @output, "	slt $isNegative.xyz, $worldNormal.xyz, $cZero				; Figure out whether each component is >0																								; LINEINFO(SDK_macros.vsh)(1472)\n";
	push @output, "	mov a0.x, $isNegative.x																								; LINEINFO(SDK_macros.vsh)(1473)\n";
	if( $add )																								
	{																								
		push @output, "		mad $linearColor.xyz, $nSquared.x, c[a0.x + $cAmbientColorPosXOffset], $linearColor			; $linearColor = normal[0]*normal[0] * box color of appropriate x side																								; LINEINFO(SDK_macros.vsh)(1476)\n";
	}																								
	else																								
	{																								
		push @output, "		mul $linearColor.xyz, $nSquared.x, c[a0.x + $cAmbientColorPosXOffset]			; $linearColor = normal[0]*normal[0] * box color of appropriate x side																								; LINEINFO(SDK_macros.vsh)(1480)\n";
	}																								
	push @output, "	mov a0.x, $isNegative.y																								; LINEINFO(SDK_macros.vsh)(1482)\n";
	push @output, "	mad $linearColor.xyz, $nSquared.y, c[a0.x + $cAmbientColorPosYOffset], $linearColor																								; LINEINFO(SDK_macros.vsh)(1483)\n";
	push @output, "	mov a0.x, $isNegative.z																								; LINEINFO(SDK_macros.vsh)(1484)\n";
	push @output, "	mad $linearColor.xyz, $nSquared.z, c[a0.x + $cAmbientColorPosZOffset], $linearColor																								; LINEINFO(SDK_macros.vsh)(1485)\n";
	&FreeRegister( \$isNegative, "\$isNegative" );																								
	&FreeRegister( \$nSquared, "\$nSquared" );																								
}																								
sub DirectionalLight																								
{																								
	local( $worldNormal ) = shift;																								
	local( $linearColor ) = shift;																								
	local( $add ) = shift;																								
	&AllocateRegister( \$nDotL, "\$nDotL" ); # FIXME: This only needs to be a scalar																								
	push @output, "	; NOTE: Gotta use -l here, since light direction = -l																								; LINEINFO(SDK_macros.vsh)(1499)\n";
	push @output, "	; DIRECTIONAL LIGHT																								; LINEINFO(SDK_macros.vsh)(1500)\n";
	push @output, "	; compute n dot l																								; LINEINFO(SDK_macros.vsh)(1501)\n";
	push @output, "	dp3 $nDotL.x, -c[a0.x + 1], $worldNormal																								; LINEINFO(SDK_macros.vsh)(1502)\n";
	if ( $g_xbox )																								
	{																								
		push @output, "		; HALF LAMBERT																								; LINEINFO(SDK_macros.vsh)(1506)\n";
		push @output, "		mad $nDotL.y, $nDotL.x, $cHalf, $cHalf  ; dot = (dot * 0.5 + 0.5)^2																								; LINEINFO(SDK_macros.vsh)(1507)\n";
		push @output, "		mul $nDotL.y, $nDotL.y, $nDotL.y																								; LINEINFO(SDK_macros.vsh)(1508)\n";
		push @output, "		max $nDotL.x, $nDotL.x, $cZero			; clamp to zero  																								; LINEINFO(SDK_macros.vsh)(1509)\n";
		push @output, "		sub $nDotL.z, $nDotL.y, $nDotL.x																								; LINEINFO(SDK_macros.vsh)(1510)\n";
		push @output, "		mad $nDotL.x, $SHADER_HALFLAMBERT, $nDotL.z, $nDotL.x																								; LINEINFO(SDK_macros.vsh)(1511)\n";
	}																								
	else																								
	{																								
		if ( $HALF_LAMBERT == 0 )																								
		{																								
			push @output, "			; lambert																								; LINEINFO(SDK_macros.vsh)(1517)\n";
			push @output, "			max $nDotL.x, $nDotL.x, c0.x				; Clamp to zero																								; LINEINFO(SDK_macros.vsh)(1518)\n";
		}																								
		elsif ( $HALF_LAMBERT == 1 )																								
		{																								
			push @output, "			; half-lambert																								; LINEINFO(SDK_macros.vsh)(1522)\n";
			push @output, "			mad $nDotL.x, $nDotL.x, $cHalf, $cHalf		; dot = (dot * 0.5 + 0.5)^2																								; LINEINFO(SDK_macros.vsh)(1523)\n";
			push @output, "			mul $nDotL.x, $nDotL.x, $nDotL.x																								; LINEINFO(SDK_macros.vsh)(1524)\n";
		}																								
		else																								
		{																								
			die "\$HALF_LAMBERT is hosed\n";																								
		}																								
	}																								
	if( $add )																								
	{																								
		push @output, "		mad $linearColor.xyz, c[a0.x], $nDotL.x, $linearColor																								; LINEINFO(SDK_macros.vsh)(1534)\n";
	}																								
	else																								
	{																								
		push @output, "		mul $linearColor.xyz, c[a0.x], $nDotL.x																								; LINEINFO(SDK_macros.vsh)(1538)\n";
	}																								
	&FreeRegister( \$nDotL, "\$nDotL" );																								
}																								
sub PointLight																								
{																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	local( $linearColor ) = shift;																								
	local( $add ) = shift;																								
	local( $lightDir );																								
	&AllocateRegister( \$lightDir, "\$lightDir" );																								
	push @output, "	; POINT LIGHT																								; LINEINFO(SDK_macros.vsh)(1554)\n";
	push @output, "	; compute light direction																								; LINEINFO(SDK_macros.vsh)(1555)\n";
	push @output, "	sub $lightDir, c[a0.x+2], $worldPos																								; LINEINFO(SDK_macros.vsh)(1556)\n";
	local( $lightDistSquared );																								
	local( $ooLightDist );																								
	&AllocateRegister( \$lightDistSquared, "\$lightDistSquared" );																								
	&AllocateRegister( \$ooLightDist, "\$ooLightDist" );																								
	push @output, "	; normalize light direction, maintain temporaries for attenuation																								; LINEINFO(SDK_macros.vsh)(1563)\n";
	push @output, "	dp3 $lightDistSquared, $lightDir, $lightDir																								; LINEINFO(SDK_macros.vsh)(1564)\n";
	push @output, "	rsq $ooLightDist, $lightDistSquared.x																								; LINEINFO(SDK_macros.vsh)(1565)\n";
	push @output, "	mul $lightDir, $lightDir, $ooLightDist.x																								; LINEINFO(SDK_macros.vsh)(1566)\n";
	local( $attenuationFactors );																								
	&AllocateRegister( \$attenuationFactors, "\$attenuationFactors" );																								
	push @output, "	; compute attenuation amount (r2 = 'd*d d*d d*d d*d', r3 = '1/d 1/d 1/d 1/d')																								; LINEINFO(SDK_macros.vsh)(1571)\n";
	push @output, "	dst $attenuationFactors, $lightDistSquared, $ooLightDist						; r4 = ( 1, d, d*d, 1/d )																								; LINEINFO(SDK_macros.vsh)(1572)\n";
	&FreeRegister( \$lightDistSquared, "\$lightDistSquared" );																								
	&FreeRegister( \$ooLightDist, "\$ooLightDist" );																								
	local( $attenuation );																								
	&AllocateRegister( \$attenuation, "\$attenuation" );																								
	push @output, "	dp3 $attenuation, $attenuationFactors, c[a0.x+4]				; r3 = atten0 + d * atten1 + d*d * atten2																								; LINEINFO(SDK_macros.vsh)(1577)\n";
	push @output, "	rcp $lightDir.w, $attenuation						; $lightDir.w = 1 / (atten0 + d * atten1 + d*d * atten2)																								; LINEINFO(SDK_macros.vsh)(1579)\n";
	&FreeRegister( \$attenuationFactors, "\$attenuationFactors" );																								
	&FreeRegister( \$attenuation, "\$attenuation" );																								
	local( $tmp );																								
	&AllocateRegister( \$tmp, "\$tmp" ); # FIXME : really only needs to be a scalar																								
	push @output, "	; compute n dot l, fold in distance attenutation																								; LINEINFO(SDK_macros.vsh)(1587)\n";
	push @output, "	dp3 $tmp.x, $lightDir, $worldNormal																								; LINEINFO(SDK_macros.vsh)(1588)\n";
	if ( $g_xbox )																								
	{																								
		push @output, "		; HALF LAMBERT																								; LINEINFO(SDK_macros.vsh)(1592)\n";
		push @output, "		mad $tmp.y, $tmp.x, $cHalf, $cHalf  ; dot = (dot * 0.5 + 0.5)^2																								; LINEINFO(SDK_macros.vsh)(1593)\n";
		push @output, "		mul $tmp.y, $tmp.y, $tmp.y																								; LINEINFO(SDK_macros.vsh)(1594)\n";
		push @output, "		max $tmp.x, $tmp.x, $cZero			; clamp to zero  																								; LINEINFO(SDK_macros.vsh)(1595)\n";
		push @output, "		sub $tmp.z, $tmp.y, $tmp.x																								; LINEINFO(SDK_macros.vsh)(1596)\n";
		push @output, "		mad $tmp.x, $SHADER_HALFLAMBERT, $tmp.z, $tmp.x																								; LINEINFO(SDK_macros.vsh)(1597)\n";
	}																								
	else																								
	{																								
		if ( $HALF_LAMBERT == 0 )																								
		{																								
			push @output, "			; lambert																								; LINEINFO(SDK_macros.vsh)(1603)\n";
			push @output, "			max $tmp.x, $tmp.x, c0.x				; Clamp to zero																								; LINEINFO(SDK_macros.vsh)(1604)\n";
		}																								
		elsif ( $HALF_LAMBERT == 1 )																								
		{																								
			push @output, "			; half-lambert																								; LINEINFO(SDK_macros.vsh)(1608)\n";
			push @output, "			mad $tmp.x, $tmp.x, $cHalf, $cHalf		; dot = (dot * 0.5 + 0.5)^2																								; LINEINFO(SDK_macros.vsh)(1609)\n";
			push @output, "			mul $tmp.x, $tmp.x, $tmp.x																								; LINEINFO(SDK_macros.vsh)(1610)\n";
		}																								
		else																								
		{																								
			die "\$HALF_LAMBERT is hosed\n";																								
		}																								
	}																								
	push @output, "	mul $tmp.x, $tmp.x, $lightDir.w																								; LINEINFO(SDK_macros.vsh)(1618)\n";
	if( $add )																								
	{																								
		push @output, "		mad $linearColor.xyz, c[a0.x], $tmp.x, $linearColor																								; LINEINFO(SDK_macros.vsh)(1621)\n";
	}																								
	else																								
	{																								
		push @output, "		mul $linearColor.xyz, c[a0.x], $tmp.x																								; LINEINFO(SDK_macros.vsh)(1625)\n";
	}																								
	&FreeRegister( \$lightDir, "\$lightDir" );																								
	&FreeRegister( \$tmp, "\$tmp" ); # FIXME : really only needs to be a scalar																								
}																								
sub SpotLight																								
{																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	local( $linearColor ) = shift;																								
	local( $add ) = shift;																								
	local( $lightDir );																								
	&AllocateRegister( \$lightDir, "\$lightDir" );																								
	push @output, "	; SPOTLIGHT																								; LINEINFO(SDK_macros.vsh)(1642)\n";
	push @output, "	; compute light direction																								; LINEINFO(SDK_macros.vsh)(1643)\n";
	push @output, "	sub $lightDir, c[a0.x+2], $worldPos																								; LINEINFO(SDK_macros.vsh)(1644)\n";
	local( $lightDistSquared );																								
	local( $ooLightDist );																								
	&AllocateRegister( \$lightDistSquared, "\$lightDistSquared" );																								
	&AllocateRegister( \$ooLightDist, "\$ooLightDist" );																								
	push @output, "	; normalize light direction, maintain temporaries for attenuation																								; LINEINFO(SDK_macros.vsh)(1651)\n";
	push @output, "	dp3 $lightDistSquared, $lightDir, $lightDir																								; LINEINFO(SDK_macros.vsh)(1652)\n";
	push @output, "	rsq $ooLightDist, $lightDistSquared.x																								; LINEINFO(SDK_macros.vsh)(1653)\n";
	push @output, "	mul $lightDir, $lightDir, $ooLightDist.x																								; LINEINFO(SDK_macros.vsh)(1654)\n";
	local( $attenuationFactors );																								
	&AllocateRegister( \$attenuationFactors, "\$attenuationFactors" );																								
	push @output, "	; compute attenuation amount (r2 = 'd*d d*d d*d d*d', r3 = '1/d 1/d 1/d 1/d')																								; LINEINFO(SDK_macros.vsh)(1659)\n";
	push @output, "	dst $attenuationFactors, $lightDistSquared, $ooLightDist						; r4 = ( 1, d, d*d, 1/d )																								; LINEINFO(SDK_macros.vsh)(1660)\n";
	&FreeRegister( \$lightDistSquared, "\$lightDistSquared" );																								
	&FreeRegister( \$ooLightDist, "\$ooLightDist" );																								
	local( $attenuation );	&AllocateRegister( \$attenuation, "\$attenuation" );																								
	push @output, "	dp3 $attenuation, $attenuationFactors, c[a0.x+4]				; r3 = atten0 + d * atten1 + d*d * atten2																								; LINEINFO(SDK_macros.vsh)(1666)\n";
	push @output, "	rcp $lightDir.w, $attenuation						; r1.w = 1 / (atten0 + d * atten1 + d*d * atten2)																								; LINEINFO(SDK_macros.vsh)(1667)\n";
	&FreeRegister( \$attenuationFactors, "\$attenuationFactors" );																								
	&FreeRegister( \$attenuation, "\$attenuation" );																								
	local( $litSrc ); &AllocateRegister( \$litSrc, "\$litSrc" );																								
	local( $tmp ); &AllocateRegister( \$tmp, "\$tmp" ); # FIXME - only needs to be scalar																								
	push @output, "	; compute n dot l																								; LINEINFO(SDK_macros.vsh)(1675)\n";
	push @output, "	dp3 $litSrc.x, $worldNormal, $lightDir																								; LINEINFO(SDK_macros.vsh)(1676)\n";
	if ( $g_xbox )																								
	{																								
		push @output, "		; HALF LAMBERT																								; LINEINFO(SDK_macros.vsh)(1680)\n";
		push @output, "		mad $litSrc.y, $litSrc.x, $cHalf, $cHalf	; dot = (dot * 0.5 + 0.5)^2																								; LINEINFO(SDK_macros.vsh)(1681)\n";
		push @output, "		mul $litSrc.y, $litSrc.y, $litSrc.y																								; LINEINFO(SDK_macros.vsh)(1682)\n";
		push @output, "		max $litSrc.x, $litSrc.x, $cZero			; clamp to zero  																								; LINEINFO(SDK_macros.vsh)(1683)\n";
		push @output, "		sub $litSrc.z, $litSrc.y, $litSrc.x																								; LINEINFO(SDK_macros.vsh)(1684)\n";
		push @output, "		mad $litSrc.x, $SHADER_HALFLAMBERT, $litSrc.z, $litSrc.x																								; LINEINFO(SDK_macros.vsh)(1685)\n";
	}																								
	else																								
	{																								
		if ( $HALF_LAMBERT == 0 )																								
		{																								
			push @output, "			; lambert																								; LINEINFO(SDK_macros.vsh)(1691)\n";
			push @output, "			max $litSrc.x, $litSrc.x, c0.x				; Clamp to zero																								; LINEINFO(SDK_macros.vsh)(1692)\n";
		}																								
		elsif ( $HALF_LAMBERT == 1 )																								
		{																								
			push @output, "			; half-lambert																								; LINEINFO(SDK_macros.vsh)(1696)\n";
			push @output, "			mad $litSrc.x, $litSrc.x, $cHalf, $cHalf	; dot = (dot * 0.5 + 0.5) ^ 2																								; LINEINFO(SDK_macros.vsh)(1697)\n";
			push @output, "			mul $litSrc.x, $litSrc.x, $litSrc.x																								; LINEINFO(SDK_macros.vsh)(1698)\n";
		}																								
		else																								
		{																								
			die "\$HALF_LAMBERT is hosed\n";																								
		}																								
	}																								
	push @output, "	; compute angular attenuation																								; LINEINFO(SDK_macros.vsh)(1706)\n";
	push @output, "	dp3 $tmp.x, c[a0.x+1], -$lightDir				; dot = -delta * spot direction																								; LINEINFO(SDK_macros.vsh)(1707)\n";
	push @output, "	sub $litSrc.y, $tmp.x, c[a0.x+3].z				; r2.y = dot - stopdot2																								; LINEINFO(SDK_macros.vsh)(1708)\n";
	&FreeRegister( \$tmp, "\$tmp" );																								
	push @output, "	mul $litSrc.y, $litSrc.y, c[a0.x+3].w			; r2.y = (dot - stopdot2) / (stopdot - stopdot2)																								; LINEINFO(SDK_macros.vsh)(1710)\n";
	push @output, "	mov $litSrc.w, c[a0.x+3].x						; r2.w = exponent																								; LINEINFO(SDK_macros.vsh)(1711)\n";
	local( $litDst ); &AllocateRegister( \$litDst, "\$litDst" );																								
	push @output, "	lit $litDst, $litSrc							; r3.y = N dot L or 0, whichever is bigger																								; LINEINFO(SDK_macros.vsh)(1713)\n";
	&FreeRegister( \$litSrc, "\$litSrc" );																								
													push @output, "													; r3.z = pow((dot - stopdot2) / (stopdot - stopdot2), exponent)																								; LINEINFO(SDK_macros.vsh)(1715)\n";
	push @output, "	min $litDst.z, $litDst.z, $cOne		 			; clamp pow() to 1																								; LINEINFO(SDK_macros.vsh)(1716)\n";
	local( $tmp1 ); &AllocateRegister( \$tmp1, "\$tmp1" );																								
	local( $tmp2 ); &AllocateRegister( \$tmp2, "\$tmp2" );  # FIXME - could be scalar																								
	push @output, "	; fold in distance attenutation with other factors																								; LINEINFO(SDK_macros.vsh)(1721)\n";
	push @output, "	mul $tmp1, c[a0.x], $lightDir.w																								; LINEINFO(SDK_macros.vsh)(1722)\n";
	push @output, "	mul $tmp2.x, $litDst.y, $litDst.z																								; LINEINFO(SDK_macros.vsh)(1723)\n";
	if( $add )																								
	{																								
		push @output, "		mad $linearColor.xyz, $tmp1, $tmp2.x, $linearColor																								; LINEINFO(SDK_macros.vsh)(1726)\n";
	}																								
	else																								
	{																								
		push @output, "		mul $linearColor.xyz, $tmp1, $tmp2.x																								; LINEINFO(SDK_macros.vsh)(1730)\n";
	}																								
	&FreeRegister( \$lightDir, "\$lightDir" );																								
	&FreeRegister( \$litDst, "\$litDst" );																								
	&FreeRegister( \$tmp1, "\$tmp1" );																								
	&FreeRegister( \$tmp2, "\$tmp2" );																								
}																								
sub DoLight																								
{																								
	local( $lightType ) = shift;																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	local( $linearColor ) = shift;																								
	local( $add ) = shift;																								
	if( $lightType eq "spot" )																								
	{																								
		&SpotLight( $worldPos, $worldNormal, $linearColor, $add );																								
	}																								
	elsif( $lightType eq "point" )																								
	{																								
		&PointLight( $worldPos, $worldNormal, $linearColor, $add );																								
	}																								
	elsif( $lightType eq "directional" )																								
	{																								
		&DirectionalLight( $worldNormal, $linearColor, $add );																								
	}																								
	else																								
	{																								
		die "don't know about light type \"$lightType\"\n";																								
	}																								
}																								
sub DoLighting																								
{																								
	if( !defined $LIGHT_COMBO )																								
	{																								
		die "DoLighting called without using \$LIGHT_COMBO\n";																								
	}																								
	if ( !$g_xbox && !defined $HALF_LAMBERT )																								
	{																								
		die "DoLighting called without using \$HALF_LAMBERT\n";																								
	}																								
	my $staticLightType = $g_staticLightTypeArray[$LIGHT_COMBO];																								
	my $ambientLightType = $g_ambientLightTypeArray[$LIGHT_COMBO];																								
	my $localLightType1 = $g_localLightType1Array[$LIGHT_COMBO];																								
	my $localLightType2 = $g_localLightType2Array[$LIGHT_COMBO];																								
#	print "\$staticLightType = $staticLightType\n";																								
#	print "\$ambientLightType = $ambientLightType\n";																								
#	print "\$localLightType1 = $localLightType1\n";																								
#	print "\$localLightType2 = $localLightType2\n";																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	push @output, "	; special case for no lighting																								; LINEINFO(SDK_macros.vsh)(1789)\n";
	if( $staticLightType eq "none" && $ambientLightType eq "none" &&																								
		$localLightType1 eq "none" && $localLightType2 eq "none" )																								
	{																								
		return;																								
	}																								
	push @output, "	; special case for static lighting only																								; LINEINFO(SDK_macros.vsh)(1796)\n";
	push @output, "	; Don't need to bother converting to linear space in this case.																								; LINEINFO(SDK_macros.vsh)(1797)\n";
	if( $staticLightType eq "static" && $ambientLightType eq "none" &&																								
		$localLightType1 eq "none" && $localLightType2 eq "none" )																								
	{																								
		push @output, "		mov oD0, $vSpecular																								; LINEINFO(SDK_macros.vsh)(1801)\n";
		return;																								
	}																								
	local( $linearColor ); &AllocateRegister( \$linearColor, "\$linearColor" );
	local( $gammaColor ); &AllocateRegister( \$gammaColor, "\$gammaColor" );
	local( $add ) = 0;																								
	if( $staticLightType eq "static" )																								
	{																								
		push @output, "		; The static lighting comes in in gamma space and has also been premultiplied by $cOverbrightFactor																								; LINEINFO(SDK_macros.vsh)(1811)\n";
		push @output, "		; need to get it into																								; LINEINFO(SDK_macros.vsh)(1812)\n";
		push @output, "		; linear space so that we can do adds.																								; LINEINFO(SDK_macros.vsh)(1813)\n";
		push @output, "		rcp $gammaColor.w, $cOverbrightFactor																								; LINEINFO(SDK_macros.vsh)(1814)\n";
		push @output, "		mul $gammaColor.xyz, $vSpecular, $gammaColor.w																								; LINEINFO(SDK_macros.vsh)(1815)\n";
		&GammaToLinear( $gammaColor, $linearColor );																								
		$add = 1;																								
	}																								
	if( $ambientLightType eq "ambient" )																								
	{																								
		&AmbientLight( $worldNormal, $linearColor, $add );																								
		$add = 1;																								
	}																								
	if( $localLightType1 ne "none" )																								
	{																								
		push @output, "		mov a0.x, $cLight0Offset																								; LINEINFO(SDK_macros.vsh)(1828)\n";
		&DoLight( $localLightType1, $worldPos, $worldNormal, $linearColor, $add );																								
		$add = 1;																								
	}																								
	if( $localLightType2 ne "none" )																								
	{																								
		push @output, "		mov a0.x, $cLight1Offset																								; LINEINFO(SDK_macros.vsh)(1835)\n";
		&DoLight( $localLightType2, $worldPos, $worldNormal, $linearColor, $add );																								
		$add = 1;																								
	}																								
	push @output, "	;------------------------------------------------------------------------------																								; LINEINFO(SDK_macros.vsh)(1840)\n";
	push @output, "	; Output color (gamma correction)																								; LINEINFO(SDK_macros.vsh)(1841)\n";
	push @output, "	;------------------------------------------------------------------------------																								; LINEINFO(SDK_macros.vsh)(1842)\n";
	&LinearToGamma( $linearColor, $gammaColor );																								
	if( 0 )																								
	{																								
		push @output, "		mul oD0.xyz, $gammaColor.xyz, $cOverbrightFactor																								; LINEINFO(SDK_macros.vsh)(1847)\n";
	}																								
	else																								
	{																								
		push @output, "		mul $gammaColor.xyz, $gammaColor.xyz, $cOverbrightFactor																								; LINEINFO(SDK_macros.vsh)(1851)\n";
		&ColorClamp( $gammaColor, "oD0" );																								
	}																								
 	push @output, ";	mov oD0.xyz, $linearColor																								; LINEINFO(SDK_macros.vsh)(1855)\n";
	push @output, "	mov oD0.w, $cOne				; make sure all components are defined																								; LINEINFO(SDK_macros.vsh)(1856)\n";
	&FreeRegister( \$linearColor, "\$linearColor" );
	&FreeRegister( \$gammaColor, "\$gammaColor" );
}																								
sub DoDynamicLightingToLinear																								
{																								
	local( $worldPos ) = shift;																								
	local( $worldNormal ) = shift;																								
	local( $linearColor ) = shift;																								
	if( !defined $LIGHT_COMBO )																								
	{																								
		die "DoLighting called without using \$LIGHT_COMBO\n";																								
	}																								
	if ( !g_xbox && !defined $HALF_LAMBERT )																								
	{																								
		die "DoLighting called without using \$HALF_LAMBERT\n";																								
	}																								
	my $staticLightType = $g_staticLightTypeArray[$LIGHT_COMBO];																								
	my $ambientLightType = $g_ambientLightTypeArray[$LIGHT_COMBO];																								
	my $localLightType1 = $g_localLightType1Array[$LIGHT_COMBO];																								
	my $localLightType2 = $g_localLightType2Array[$LIGHT_COMBO];																								
	# No lights at all. . note that we don't even consider static lighting here.																								
	if( $ambientLightType eq "none" &&																								
		$localLightType1 eq "none" && $localLightType2 eq "none" )																								
	{																								
		push @output, "		mov $linearColor, $cZero																								; LINEINFO(SDK_macros.vsh)(1886)\n";
		return;																								
	}																								
	local( $add ) = 0;																								
	if( $ambientLightType eq "ambient" )																								
	{																								
		&AmbientLight( $worldNormal, $linearColor, $add );																								
		$add = 1;																								
	}																								
	if( $localLightType1 ne "none" )																								
	{																								
		push @output, "		mov a0.x, $cLight0Offset																								; LINEINFO(SDK_macros.vsh)(1899)\n";
		&DoLight( $localLightType1, $worldPos, $worldNormal, $linearColor, $add );																								
		$add = 1;																								
	}																								
	if( $localLightType2 ne "none" )																								
	{																								
		push @output, "		mov a0.x, $cLight1Offset																								; LINEINFO(SDK_macros.vsh)(1906)\n";
		&DoLight( $localLightType2, $worldPos, $worldNormal, $linearColor, $add );																								
		$add = 1;																								
	}																								
}																								
sub NotImplementedYet																								
{																								
	&AllocateRegister( \$projPos, "\$projPos" );																								
	push @output, "	dp4 $projPos.x, $worldPos, $cViewProj0																								; LINEINFO(SDK_macros.vsh)(1915)\n";
	push @output, "	dp4 $projPos.y, $worldPos, $cViewProj1																								; LINEINFO(SDK_macros.vsh)(1916)\n";
	push @output, "	dp4 $projPos.z, $worldPos, $cViewProj2																								; LINEINFO(SDK_macros.vsh)(1917)\n";
	push @output, "	dp4 $projPos.w, $worldPos, $cViewProj3																								; LINEINFO(SDK_macros.vsh)(1918)\n";
	push @output, "	mov oPos, $projPos																								; LINEINFO(SDK_macros.vsh)(1919)\n";
	&FreeRegister( \$projPos, "\$projPos" );																								
	exit;																								
}																								
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(14)\n";
push @output, "; Vertex blending																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(15)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(16)\n";
&AllocateRegister( \$worldPos, "\$worldPos" );																								
&AllocateRegister( \$worldNormal, "\$worldNormal" );																								
&AllocateRegister( \$worldTangentS, "\$worldTangentS" );																								
&AllocateRegister( \$worldTangentT, "\$worldTangentT" );																								
&SkinPositionNormalAndTangentSpace( $worldPos, $worldNormal, 																								
					$worldTangentS, $worldTangentT );																								
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(26)\n";
push @output, "; Transform the position from world to proj space																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(27)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(28)\n";
&AllocateRegister( \$projPos, "\$projPos" );																								
push @output, "dp4 $projPos.x, $worldPos, $cViewProj0																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(32)\n";
push @output, "dp4 $projPos.y, $worldPos, $cViewProj1																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(33)\n";
push @output, "dp4 $projPos.z, $worldPos, $cViewProj2																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(34)\n";
push @output, "dp4 $projPos.w, $worldPos, $cViewProj3																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(35)\n";
push @output, "mov oPos, $projPos																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(36)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(38)\n";
push @output, "; Fog																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(39)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(40)\n";
&CalcFog( $worldPos, $projPos );																								
&FreeRegister( \$projPos, "\$projPos" );																								
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(45)\n";
push @output, "; Lighting																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(46)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(47)\n";
push @output, "; Transform tangent space basis vectors to env map space (world space)																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(49)\n";
push @output, "; This will produce a set of vectors mapping from tangent space to env space																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(50)\n";
push @output, "; We'll use this to transform normals from the normal map from tangent space																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(51)\n";
push @output, "; to environment map space. 																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(52)\n";
push @output, "; NOTE: use dp3 here since the basis vectors are vectors, not points																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(53)\n";
push @output, "; svect																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(55)\n";
push @output, "mov oT1.x, $worldTangentS.x																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(56)\n";
push @output, "mov oT2.x, $worldTangentS.y																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(57)\n";
push @output, "mov oT3.x, $worldTangentS.z																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(58)\n";
&FreeRegister( \$worldTangentS, "\$worldTangentS" );																								
push @output, "; tvect																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(61)\n";
push @output, "mov oT1.y, $worldTangentT.x																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(62)\n";
push @output, "mov oT2.y, $worldTangentT.y																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(63)\n";
push @output, "mov oT3.y, $worldTangentT.z																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(64)\n";
&FreeRegister( \$worldTangentT, "\$worldTangentT" );																								
push @output, "; normal																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(67)\n";
push @output, "mov oT1.z, $worldNormal.x																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(68)\n";
push @output, "mov oT2.z, $worldNormal.y																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(69)\n";
push @output, "mov oT3.z, $worldNormal.z																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(70)\n";
&FreeRegister( \$worldNormal, "\$worldNormal" );																								
push @output, "; Compute the vector from vertex to camera																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(74)\n";
&AllocateRegister( \$eyeVector, "\$eyeVector" );																								
push @output, "sub $eyeVector.xyz, $cEyePos, $worldPos  																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(76)\n";
&FreeRegister( \$worldPos, "\$worldPos" );																								
push @output, "; Move it into the w component of the texture coords, as the wacky																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(80)\n";
push @output, "; pixel shader wants it there.																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(81)\n";
push @output, "mov oT1.w, $eyeVector.x																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(82)\n";
push @output, "mov oT2.w, $eyeVector.y																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(83)\n";
push @output, "mov oT3.w, $eyeVector.z																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(84)\n";
&FreeRegister( \$eyeVector, "\$eyeVector" );																								
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(88)\n";
push @output, "; Texture coordinates																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(89)\n";
push @output, ";------------------------------------------------------------------------------																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(90)\n";
push @output, "dp4 oT0.x, $vTexCoord0, $SHADER_SPECIFIC_CONST_4																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(91)\n";
push @output, "dp4 oT0.y, $vTexCoord0, $SHADER_SPECIFIC_CONST_5																								; LINEINFO(SDK_VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh)(92)\n";
