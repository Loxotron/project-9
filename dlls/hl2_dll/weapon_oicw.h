//========= Copyright � 1996-2007, DetLeR, Valve Corporation, All rights reserved. ============//
//
// Purpose: Projectile shot from the OICW
// ## Public Code ##
// $Workfile: $
// $Date: $
//
//----------------------------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//==============================================================================================//

#ifndef WEAPONOICW_H
#define WEAPONOICW_H

#include "basegrenade_shared.h"
#include "basehlcombatweapon.h"

class CWeaponOICW : public CHLMachineGun
{
public:
	DECLARE_CLASS( CWeaponOICW, CHLMachineGun );

	CWeaponOICW();

	DECLARE_SERVERCLASS();

	void ItemPostFrame( void );
	float GetFireRate( void );
	void Drop( const Vector &velocity );
	void Precache( void );
	//void DelayedAttack( void );
	//void PrimaryAttack( void );
	void SecondaryAttack( void );

	void AddViewKick( void );

	void FireNPCPrimaryAttack( CBaseCombatCharacter *pOperator, bool bUseWeaponAngles );
	void FireNPCSecondaryAttack( CBaseCombatCharacter *pOperator, bool bUseWeaponAngles );
	void Operator_HandleAnimEvent( animevent_t *pEvent, CBaseCombatCharacter *pOperator );

	int GetMinBurst( void ) { return 2; }
	int GetMaxBurst( void ) { return 5; }

	bool Holster( CBaseCombatWeapon *pSwitchingTo = NULL );
	bool Reload( void );
	bool Deploy( void );
	void Zoom( void );
	int CapabilitiesGet( void ) { return bits_CAP_WEAPON_RANGE_ATTACK1; }
	
	void	SendGrenageUsageState( void ); // VXP
	void	UseGrenade( bool use ); // VXP

	Activity GetPrimaryAttackActivity( void );

	virtual const Vector& GetBulletSpread( void )
	{
		static Vector cone;

		cone = VECTOR_CONE_3DEGREES;

		return cone;
	}

protected:

	float m_flDelayedFire;
	bool m_bShotDelayed;
	bool m_bZoomed;
	//int m_nVentPose;

	bool m_bUseGrenade;

	DECLARE_ACTTABLE();
	DECLARE_DATADESC();
};

#endif //WEAPONAR2_H 