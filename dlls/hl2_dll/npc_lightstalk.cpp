//========= Copyright � 1999, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "actanimating.h"
#include "Sprite.h"
#include "EventQueue.h"

#define XEN_PLANT_GLOW_SPRITE		"sprites/flare3.vmt"
#define XEN_PLANT_HIDE_TIME			5

class CXenPLight : public CActAnimating
{
	DECLARE_CLASS( CXenPLight, CActAnimating );

public:

	DECLARE_DATADESC();

	void		Spawn( void );
	void		Precache( void );
	void		Touch( CBaseEntity *pOther );
	void		Think( void );

	void		LightOn( void );
	void		LightOff( void );

	float		m_flDmgTime;
	

private:
	CSprite		*m_pGlow;
};

LINK_ENTITY_TO_CLASS( npc_lightstalk, CXenPLight );

BEGIN_DATADESC( CXenPLight )
	DEFINE_FIELD( m_pGlow, FIELD_CLASSPTR ),
	DEFINE_FIELD( m_flDmgTime, FIELD_FLOAT ),
END_DATADESC()

void CXenPLight::Spawn( void )
{
	Precache();

	SetModel( "models/light.mdl" );

	SetMoveType( MOVETYPE_NONE );
	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_TRIGGER | FSOLID_NOT_SOLID );

	UTIL_SetSize( this, Vector(-80,-80,0), Vector(80,80,32));
	SetActivity( ACT_IDLE );
	SetNextThink( gpGlobals->curtime + 0.1 );
	SetCycle( random->RandomFloat(0,1) );

	m_pGlow = CSprite::SpriteCreate( XEN_PLANT_GLOW_SPRITE, GetLocalOrigin() + Vector(0,0,(WorldAlignMins().z+WorldAlignMaxs().z)*0.5), FALSE );
	m_pGlow->SetTransparency( kRenderGlow, GetRenderColor().r, GetRenderColor().g, GetRenderColor().b, GetRenderColor().a, m_nRenderFX );
	m_pGlow->SetAttachment( this, 1 );
}


void CXenPLight::Precache( void )
{
	PrecacheModel( "models/light.mdl" );
	PrecacheModel( XEN_PLANT_GLOW_SPRITE );
}


void CXenPLight::Think( void )
{
	StudioFrameAdvance();
	SetNextThink( gpGlobals->curtime + 0.1 );

	switch( GetActivity() )
	{
	case ACT_CROUCH:
		if ( IsSequenceFinished() )
		{
			SetActivity( ACT_CROUCHIDLE );
			LightOff();
		}
		break;

	case ACT_CROUCHIDLE:
		if ( gpGlobals->curtime > m_flDmgTime )
		{
			SetActivity( ACT_STAND );
			LightOn();
		}
		break;

	case ACT_STAND:
		if ( IsSequenceFinished() )
			SetActivity( ACT_IDLE );
		break;

	case ACT_IDLE:
	default:
		break;
	}
}


void CXenPLight::Touch( CBaseEntity *pOther )
{
	if ( pOther->IsPlayer() )
	{
		m_flDmgTime = gpGlobals->curtime + XEN_PLANT_HIDE_TIME;
		if ( GetActivity() == ACT_IDLE || GetActivity() == ACT_STAND )
		{
			SetActivity( ACT_CROUCH );
		}
	}
}


void CXenPLight::LightOn( void )
{
	variant_t Value;
	g_EventQueue.AddEvent( STRING( m_target ), "TurnOn", Value, 0, this, this );

	if ( m_pGlow )
	     m_pGlow->RemoveEffects( EF_NODRAW );
}


void CXenPLight::LightOff( void )
{
	variant_t Value;
	g_EventQueue.AddEvent( STRING( m_target ), "TurnOff", Value, 0, this, this );

	if ( m_pGlow )
		 m_pGlow->AddEffects( EF_NODRAW );
}