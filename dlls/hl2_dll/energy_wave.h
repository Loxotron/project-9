//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef ENERGYWAVE_H
#define ENERGYWAVE_H
#ifdef _WIN32
#pragma once
#endif

#include "basecombatweapon.h"
#include "energy_wave.h"


//-----------------------------------------------------------------------------
// Purpose: Shield
//-----------------------------------------------------------------------------
class CEnergyWave : public CBaseEntity
{
public:
	DECLARE_CLASS( CEnergyWave, CBaseEntity );

	DECLARE_DATADESC();
	DECLARE_SERVERCLASS();

	CEnergyWave::CEnergyWave( void )
	{
		//m_bEmit				= false;

	// Send to the client even though we don't have a model
	AddEFlags( EFL_FORCE_CHECK_TRANSMIT );
	}

public:
	void Spawn( void );
	void Precache( void );

public:
	static CEnergyWave* Create( CBaseEntity *pentOwner );
	//CNetworkVar( bool, m_bEmit );
};


#endif //ENERGYWAVE_H